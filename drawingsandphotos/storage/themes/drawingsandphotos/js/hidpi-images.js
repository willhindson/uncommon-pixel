$(function(){

	var hires = (function () {
	    "use strict";
	
	    var selector = '.hires-image[data-src]',
	        minDevicePixelRatio = 1.5,
	        $images,
	
	        devicePixelRatio = (function () {
	            if (window.devicePixelRatio === undefined) {
	                return 1;
	            }
	            return window.devicePixelRatio;
	        }()),
	
	        hiresRequired = devicePixelRatio >= minDevicePixelRatio,
	
	        loadImages = function () {
	
	            var $imgContainer,
	                $img,
	                imageSrc,
	                newImageSrc;
	
	            $images.each(function (key, imgContainer) {
	
	                $imgContainer = $(imgContainer);
	
	                if (hiresRequired) {
	                    // insert hires image
	                    newImageSrc = $imgContainer.attr('data-hires-src');
	                } else {
	                    // insert regular image
	                    newImageSrc = $imgContainer.attr('data-src');
	                }
	
	                $imgContainer.append(
	                    $("<img />")
	                    .attr('src', newImageSrc)
	                    .attr('width', '100%')
	                    .attr('height', '100%')
	                );
	            });
	
	            return this;
	        },
	
	        init = function () {
	            $images = $(selector);
	            loadImages();
	        };
	
	    init();
	
	    return {
	        loadImages : loadImages
	    };
	
	}());
	
});