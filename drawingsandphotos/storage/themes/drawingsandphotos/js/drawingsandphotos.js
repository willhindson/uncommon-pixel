//  ---------------------------------------------------------------------------------------
//  Global variables
//  ---------------------------------------------------------------------------------------

//window.j_grid = $('.grid').val();

/*
ways of triggering a window resize event (other than resizing the window):
    // $(window).resize();
    // $(window).trigger('resize');
    // window.dispatchEvent(new Event('resize'));
*/




//  ---------------------------------------------------------------------------------------
//  On window load
//  ---------------------------------------------------------------------------------------

$(window).load(function() {

    //  Load Masonry on load if small grid detected
    //  ---------------------------------------------------------------------------------------

    var j_grid = $('.grid');

    if(j_grid.hasClass('grid--small')){
        j_grid.masonry({
            //columnWidth: 200,
            itemSelector: '.grid__item'
        });
    }

});




//  ---------------------------------------------------------------------------------------
//  On window resize
//  ---------------------------------------------------------------------------------------

// causeRepaintsOn = $('h1, h2, h3, p');

$(window).resize(function() {
    //causeRepaintsOn.css("z-index", 1);
});





//  ---------------------------------------------------------------------------------------
//  On document ready:
//  ---------------------------------------------------------------------------------------
$(document).ready(function() {


    // Elements
    //  ---------------------------------------------------------------------------------------

    var j_logo = $('.heading-logo');

    var j_grid = $('.grid');

    var j_btnViewLarge = $('.js-btn-view--large');
    var j_btnViewSmall = $('.js-btn-view--small');



    //  Masonry
    //  ---------------------------------------------------------------------------------------

    function initMasonry() {

        j_grid.masonry();

        //var j_grid = $('.grid');

        // layout Masonry again after all images have loaded
        j_grid.imagesLoaded( function() {

            console.log('loaded');
            $(window).trigger('resize');
        });

        j_grid.masonry( 'on', 'layoutComplete', function( msnryInstance, laidOutItems ) {

            console.log('layout complete');
        });

    }

    // $(window).afterResize( function() {
    //      //console.log('Resize event has finished');

    //     if(j_grid.hasClass('grid--small')) {
    //         j_grid.masonry();
    //         console.log('after resize');
    //     }

    //     //j_grid.animate( {opacity: 1 }, 500);
    // }, true, 100 );

    // Single image loaded
    $(window).on('k-image-loaded', function() {
        // Lazy loaded image is loaded!
        if(j_grid.hasClass('grid--small')) {
            j_grid.masonry();
        }

        j_grid.animate( {opacity: 1 }, 300);
    });

    // Image set loaded
    $(window).on('k-infinite-loaded', function() {

    });



    // Grid view toggle
    //  ---------------------------------------------------------------------------------------

    // Set initial grid, btn states and local storage variables based on grid small or large

    if($.cookie('viewState') === 'gridSmall') {
        // If the small grid was previously selected
        //console.log($.totalStorage('viewState'));
        j_grid.addClass('grid--small');
        j_btnViewSmall.addClass('is-selected');

    } else if($.cookie('viewState') === 'gridLarge') {
        // If the large grid was previously selected
        j_grid.addClass('grid--large');
        j_btnViewLarge.addClass('is-selected');

    } else {
        // A view state button has never been clicked before
        j_grid.addClass('grid--large');
        j_btnViewLarge.addClass('is-selected');
    }

    /*if(j_grid.hasClass('grid--small')) {
        j_btnViewSmall.addClass('is-selected');
    } else { // large
        j_btnViewLarge.addClass('is-selected');
    }*/

    function changeGridToLarge() {
        if(j_grid.hasClass('grid--small')) {
            $(this).addClass('is-selected');
            j_btnViewSmall.removeClass('is-selected');

            j_grid.animate({ opacity: 0 }, 300, 'easeInOutExpo', function() {
                j_grid.removeClass('grid--small').addClass('grid--large');
                j_grid.masonry('destroy');
                $(window).resize();
                //j_grid.animate( {opacity: 1 }, 500);
            });

            $.cookie('viewState', 'gridLarge', { path: '/' });
        }
    }

    function changeGridToSmall() {
        if(j_grid.hasClass('grid--large')) {
            $(this).addClass('is-selected');
            j_btnViewLarge.removeClass('is-selected');

            j_grid.animate({ opacity: 0 }, 300, 'easeInOutExpo', function() {
                j_grid.removeClass('grid--large').addClass('grid--small');
                initMasonry();
            });

            $.cookie('viewState', 'gridSmall', { path: '/' });
        }
    }

    // Click on large view button (list)
    j_btnViewLarge.click(function(){
        changeGridToLarge();
    });

    // Click on small view button (grid)
    j_btnViewSmall.click(function(){
        changeGridToSmall();
    });


    //  View toggle state stored in totalStorage (HTML5 local storage plugin)
    //  ---------------------------------------------------------------------------------------

    // Set value
    //$.totalStorage('viewState', 'gridSmall');
    //$.totalStorage('viewState', 'gridLarge');

    // Get value
    //$.totalStorage('viewState');








    //  Get rid of delay on tapping on mobile browsers
    //  ---------------------------------------------------------------------------------------
    //FastClick.attach(document.body);


    $(".heading-logo").fitText(1.25, { minFontSize: '50px', maxFontSize: '140px' });
    $(".heading-site-description").fitText(2, { minFontSize: '26px', maxFontSize: '44px' });
    $("h3").fitText(3, { minFontSize: '20px', maxFontSize: '30px' });

    //$(".logo__ampersand").fitText(2);




    // SVG / PNG fallback
    //  ---------------------------------------------------------------------------------------
    if(!Modernizr.svg) {
        $('img[src*="svg"]').attr('src', function() {
            return $(this).attr('src').replace('.svg', '.png');
        });
    }


});