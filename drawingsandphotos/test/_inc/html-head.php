<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set mobile scaling -->
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

    <title>Drawings & Photos</title>

    <!--[if lt IE 9]><link rel="stylesheet" href="css/drawingsandphotos.css" /><![endif]-->
    <!--[if IE 9 ]><link rel="stylesheet" href="css/drawingsandphotos.css" /><![endif]-->
    <!--[if IE 10 ]><link rel="stylesheet" href="css/drawingsandphotos.css" /><![endif]-->
    <!--[if !IE ]><!--><link rel="stylesheet" href="css/drawingsandphotos.css" /><!--<![endif]-->

    <script src="js/jquery.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/modernizr.min.js"></script>

    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.fittext-min.js"></script>
    <script src="js/fastclick.js"></script>

    <script type="text/javascript" src="//use.typekit.net/bks7thc.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>

    <!-- D&P js -->
    <script src="js/drawingsandphotos.js"></script>
</head>

<body class="<?php echo $level; ?> <?php echo $page; ?>">

    <header class="header  l-header">
        <!-- <nav class="nav-top  l-nav-top">

        </nav> -->

        <!-- <button class="btn-nav  l-btn-nav  js-btn-toggle-nav">Menu</button> -->

        <a class="logo  l-logo" href="index.php">
            <h1 class="logo__heading">Drawings <span class="logo__ampersand">&</span> Photos</h1>
        </a>

        <h2 class="heading-site-description">Things that I made<!-- ; sometimes they are combined -->. <a href="http://www.twitter.com/willhindson" title="@willhindson">Let me know</a> if any of them would look appropriate on your wall.</h2>
    </header>