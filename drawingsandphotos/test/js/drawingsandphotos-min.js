// CONTENTS -------------------------------------------------------------------------------
/*
• Global variables

*///  ---------------------------------------------------------------------------------------
//  ---------------------------------------------------------------------------------------
//  On window load
//  ---------------------------------------------------------------------------------------
$(window).load(function(){});$(document).ready(function(){var e=$(".logo");$(".logo__heading").fitText(1.1,{minFontSize:"50px",maxFontSize:"140px"});$("h2").fitText(3,{minFontSize:"30px",maxFontSize:"40px"});$("h3").fitText(3,{minFontSize:"20px",maxFontSize:"30px"});Modernizr.svg||$('img[src*="svg"]').attr("src",function(){return $(this).attr("src").replace(".svg",".png")})});