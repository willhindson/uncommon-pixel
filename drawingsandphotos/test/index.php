<?php
    $level = 'level-stats';
    $page = 'page-dashboard';
    include ('_inc/html-head.php');
?>

<div class="l-content">
    <ul class="list-content">
        <li>
            <figure>
                <figcaption>#1</figcaption>
                <img src="img/string.jpg">
            </figure>
        </li>
        </li>

        <li>
            <figure>
                <figcaption>#2</figcaption>
                <img src="img/space-helmets.jpg">
            </figure>
        </li>

        <li>
            <figure>
                <figcaption>#3</figcaption>
                <img src="img/IMG_5116.jpg">
            </figure>
        </li>

        <!-- <li>
            <figure>
                <figcaption>#4</figcaption>
                <img src="img/woven-red.jpg">
            </figure>
        </li> -->

        <li>
            <figure>
                <figcaption>#4</figcaption>
                <img src="img/polaroid.jpg">
            </figure>
        </li>

        <li>
            <figure>
                <figcaption>#5</figcaption>
                <img src="img/bridge.jpg">
            </figure>
        </li>
    </ul>
</div>

<?php include ('_inc/html-foot.php') ?>