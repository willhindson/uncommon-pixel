/*	------------------------------------------------------------------------
 	Set up variables + functions
	------------------------------------------------------------------------ */

	// Colour pairs:
	var colours = [
		{"colour1":"#000000", "colour2":"#ffffff"}, // black - white
		{"colour1":"#0B4F63", "colour2":"#96FA89"}, // blue - green
		{"colour1":"#F8DF3A", "colour2":"#41473B"}, // bright yellow - greyblue
		{"colour1":"#FEA18B", "colour2":"#16490A"}, // skin pink - deep green
		{"colour1":"#D1ECDA", "colour2":"#CB5905"}, // milk - orange
		{"colour1":"#FE5437", "colour2":"#C8F320"}, // orange - acid yellow
		{"colour1":"#D9D894", "colour2":"#2F6EEF"}, // cream - blue
		{"colour1":"#0B0317", "colour2":"#35C8F5"}, // dark blue / light blue
		{"colour1":"#DAFD1F", "colour2":"#D04E1D"}, // lime green + orange
		{"colour1":"#067F71", "colour2":"#EEC731"}, // aquamarine + mustard
		{"colour1":"#5B01AB", "colour2":"#EBC211"} // Purple + yellow
	];

	var randomPaletteNo = (Math.floor(Math.random() * colours.length));
	var randomColours = (colours[randomPaletteNo]);

	var colour1 = randomColours.colour1;
	var colour2 = randomColours.colour2;

	// ----- FOR TESTING OUT COLOURS ----- //
	// var colour1 = 'rgb(' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ')';
	// var colour2 = 'rgb(' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ')';


/*	------------------------------------------------------------------------
 	Document ready
	------------------------------------------------------------------------ */

$(function(){

	// If the colours are b+w, change the text :)
	if(randomPaletteNo == '0'){
		$('h2 span strong').html('greyscale');
	}

	// Apply random dark-ish backgrounds behind icons
	$('.icon, .icon-shuffle').each(function(index) {
		var randomOpacity = ((Math.floor(Math.random()*100)) / 100) + 0.55;
		$(this).css({'background-color': colour1, 'opacity': randomOpacity});
	});

	// Separate letters in h1 into individual spans
	$("h1").lettering();

	// Apply generated colours
	$("h1, footer").css({'background-color':colour1});
	$("h1").css({'color':colour2}).fadeIn(1000, 'easeOutQuad');
	$("h2").css({'color':colour1 , 'background-color':colour2}).fadeIn(3000, 'easeInQuad');
	$(".icon, .icon-shuffle").css({'color':colour2});

	var i = 0;
	//var counter = document.getElementById("counter");

	$('body').css({'background-color': colour2}).fadeIn(1000);

	function generateSquareLoop() {
	     if(i < 400){
	          var randomOpacity = (Math.floor(Math.random()*100)) / 100;
	          var square = $('<div class="small-square"></div>').css({'background-color':colour1, 'opacity': randomOpacity}).fadeIn(100);
	          $(".square-bg").append(square);
	          // var squareHeight = $("div.small-square").width();
	          // $("div.small-square").css({'height': squareHeight});

	          i++;
	          setTimeout(generateSquareLoop, 0);
	     }
	}

	generateSquareLoop();


	// 	PAGE RESIZE LISTENER
	//	---------------------------------------------------------------------------------------

    /*
    // Calculate proportionate height of squares
	function calculateSquareHeights() {
	    $('.small-square, header, h2').each(function(index) {
	    	var smallSquareHeight = ($(this).width());
			$(this).css({'height':smallSquareHeight});
	    });
    }
	*/

	// Calculate vert position of h2 text in square
	function calculateBoxPos() {
		var h1Height = $('h1').outerWidth();
		var h1HeightCeil = Math.floor(h1Height);
		$('h1').css({'height':h1HeightCeil});

		var h2Height = $('h2').outerWidth();
		var h2HeightCeil = Math.floor(h2Height);
		var h2SpanHeight = $('h2 span').height();
		var h2SpanVertPos = (h2HeightCeil / 2) - (h2SpanHeight / 2);
		$('h2').css({'height':h2HeightCeil});
		$('h2 span').css({'top': h2SpanVertPos});

		var iconHeight = $('.icon').outerWidth();
		var iconHeightCeil = Math.floor(iconHeight);
		$('.icon').css({'height':iconHeightCeil});

		var iconShuffle1Height = $('.icon-shuffle.shuffle-1').outerWidth();
		var iconShuffle1HeightCeil = Math.floor(iconShuffle1Height);
		$('.icon-shuffle.shuffle-1').css({'height':iconShuffle1HeightCeil});

		var iconShuffle2Height = $('.icon-shuffle.shuffle-2').outerWidth();
		var iconShuffle2HeightCeil = Math.floor(iconShuffle2Height);
		$('.icon-shuffle.shuffle-2').css({'height':iconShuffle2HeightCeil});
	}

    // What to calculate when resize is fired
	var resize = function() {
		//calculateSquareHeights();
		calculateBoxPos();
	};

	// Fire the resizing once on load
	resize();

	// Fire continually on resize
	$(window).resize(resize);


	// 	ICON HOVERS
	//	---------------------------------------------------------------------------------------

	$(".icon, .icon-shuffle").hover(function () {
		$(this).css({'background-color': colour2, 'color': colour1});
	}, function () {
		$(this).css({'background-color': colour1, 'color': colour2});
	});

	// 	H1 RESPONSIVE RESIZE
	//	---------------------------------------------------------------------------------------
	$('h1').fitText(0.30);
	$('.icon, .icon-shuffle').fitText(0.22);

});

