<!DOCTYPE html>
    <head>
        <meta charset="UTF-8">
        <title>UP · Will Hindson</title>

        <meta name="title" content="Uncommon Pixel, miscellaneous creative stuff from Will Hindson" />
        <!-- <meta name="viewport" content="initial-scale=1.0" /> -->
        <!-- Set mobile scaling -->
        <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

        <script type="text/javascript" src="//use.typekit.net/zwm8wwv.js"></script>
        <script type="text/javascript">try{Typekit.load();}catch(e){}</script>

        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="js/jquery.fittext.js"></script>
        <script type="text/javascript" src="js/jquery.lettering.js"></script>
        <script type="text/javascript" src="js/uncommonpixel-min.js"></script>

        <link rel="stylesheet" href="css/uncommonpixel.css" />

        <!-- ANALYTICS -->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-23326595-1', 'uncommonpixel.com');
            ga('send', 'pageview');
        </script>
    </head>

    <body>
        <header>
            <h1>Uncommon Pixel</h1>
            <h2><span>Pushing dots of <strong>coloured</strong> light across electronic displays since mid way through the last decade.</span></h2>
        </header>

        <a href="javascript:history.go(0)" title="" class="icon-shuffle shuffle-1"></a>

        <section class="icons">
            <a href="http://photo.uncommonpixel.com" title=""           class="icon icon-camera"></a>
            <a href="http://doodles.uncommonpixel.com" title=""         class="icon icon-pencil-1"></a>
            <a href="http://www.flickr.com/willhindson" title=""        class="icon icon-flickr"></a>
            <a href="http://www.dribbble.com/willhindson" title=""      class="icon icon-dribbble"></a>
            <!-- <a href="http://snaps.uncommonpixel.com" title=""           class="icon icon-camera-1"></a> -->
            <a href="http://soundcloud.com/will-hindson" title=""       class="icon icon-soundcloud"></a>
            <a href="http://www.twitter.com/willhindson" title=""       class="icon icon-twitter"></a>
            <a href="http://www.linkedin.com/in/willhindson" title=""   class="icon icon-linkedin"></a>
            <a href="mailto:will@uncommonpixel.com" title=""            class="icon icon-post"></a>
        </section>

        <a href="javascript:history.go(0)" title="" class="icon-shuffle shuffle-2"></a>

        <div class="square-bg"></div>
        <!-- <a href="http://open.spotify.com/user/widster" title=""    class="icon icon-spotify"></a> -->
        <!-- <a href="http://www.flickr.com/willhindson" title="" class="icon-flickr"></a> -->
        <!-- <a href="" title="" class="icon-pencil-2"></a> -->
        <!-- <a href="" title="" class="icon-refresh"></a> -->
        <!-- <a href="" title="" class="icon-refresh-1"></a> -->
        <!-- <footer></footer> -->
    </body>
</html>