$(function(){

	/* Misc Setup & Fixes ------------------------------------------------------------------------------------ */
	/* ------------------------------------------------------------------------------------------------------- */

	// Shorten page title to fit iOS homescreen bookmark - also changes page title for Mobile Safari :( oh well
	if( navigator.userAgent.match(/iPhone/i) || 
		navigator.userAgent.match(/iPod/i) || 
		navigator.userAgent.match(/iPad/i)
		) {
		document.title = "U/P – W/H";
	}

	// Open links (with rel="ext") in new window - disabled as probably annoying, maybe implement toggle button?
	/* $("a[rel='ext']").attr("target","_blank"); */ 
			
	// "Lettering" JS plugin
	$(".lettering").lettering();
	$(".lettering_words").lettering('words').children('span').lettering();
	
	// IE cleartype antialias fix
	$("h1, h2, h3, h4, h5, p").ieffembedfix();	
	
	// Fade in each section as the page loads
	if($.browser.msie && $.browser.version < 9){
		// Silence
	} else {	
		//$('section, header, footer').css({"opacity":"0"});
		$('header, section, footer').each(function(i){
	        $(this).delay(i*150).animate({"opacity":"1"},{"opacity":"0", "filter": ""}, {duration:300});
		});
	}
	
	// Detect iPhone / iPod
	function isiPhone(){
	    return (
	        (navigator.platform.indexOf("iPhone") != -1) ||
	        (navigator.platform.indexOf("iPod") != -1)
	    );
	}
	
	/* #zero - Hover animations ------------------------------------------------------------------------------ */
	/* ------------------------------------------------------------------------------------------------------- */
	
	$("#dslr").hover(dslrOver, dslrOut);	
	function dslrOver(){
		var randomNumber= (Math.floor(Math.random()*21) -10);
		$("#dslr").stop(true, false).animate({"top": "-20px", "rotate": randomNumber}, 400, "easeOutBack");
		$("#twitterbird").stop(true, false).animate({"bottom": "-255px"}, 100, "easeInOutExpo");
		
		if($.browser.msie && $.browser.version < 9){
			$("#zero p#text-dslr").show().stop(true, false).css({"top": "205px"}, 400, "easeOutBack");
		} else {
			$("#zero p#text-dslr").show().stop(true, false).animate({"top": "205px", "opacity": "1"}, 400, "easeOutBack");
		}
	}
	function dslrOut(){
		$("#dslr").stop(true, false).animate({"top": "-98px", "rotate": "0"}, 700, "easeOutExpo");
		$("#twitterbird").stop(true, false).animate({"bottom": "-202px"}, 1000, "easeInExpo");
		
		if($.browser.msie && $.browser.version < 9){
			$("#zero p#text-dslr").stop(true, false).css({"top": "200px"}).hide();
		} else {
			$("#zero p#text-dslr").stop(true, false).animate({"top": "200px", "opacity": "hide"}, 400, "easeInOutExpo"); //use callback function instead w/ opacity=0?
		}
	}

	$("#iphone").hover(iphoneOver, iphoneOut);	
	function iphoneOver(){
		$("#iphone").stop(true, false).animate({"padding-top": "70px"}, 400, "easeOutBack");
		$("#twitterbird").stop(true, false).animate({"bottom": "-255px"}, 100, "easeInOutExpo");
		
		if($.browser.msie && $.browser.version < 9){
			$("#zero p#text-iphone ").show().stop(true, false).css({"top": "205px"});
		} else {
			$("#zero p#text-iphone ").show().stop(true, false).animate({"top": "205px", "opacity": "1", "filter": ""}, 400, "easeOutBack");
		}
	}
	function iphoneOut(){
		$("#iphone").stop(true, false).animate({"padding-top": "0"}, 700, "easeOutExpo");
		$("#twitterbird").stop(true, false).animate({"bottom": "-202px"}, 1000, "easeInExpo");
		
		if($.browser.msie && $.browser.version < 9){
			$("#zero p#text-iphone").stop(true, false).css({"top": "200px"}).hide(); 
		} else {
			$("#zero p#text-iphone").stop(true, false).animate({"top": "200px", "opacity": "hide", "filter": ""}, 400, "easeInOutExpo"); //use callback function instead w/ opacity=0?
		}
		
	}

	$("#twitterbird").hover(twitterbirdOver, twitterbirdOut);
	function twitterbirdOver(){
		$("#dslr").stop(true, false).animate({"top": "-138px"}, 300, "easeInOutExpo");
		$("#iphone").stop(true, false).animate({"top": "-85px"}, 350, "easeInOutExpo");
		$("#twitterbird").stop(true, false).animate({"bottom": "-10px"}, 400, "easeInOutExpo");
			
		if($.browser.msie && $.browser.version < 9){
			$("#zero p#text-twitter").show().stop(true, false).delay(200).css({"top": "205px"});
		} else {
			$("#zero p#text-twitter").show().stop(true, false).delay(200).animate({"top": "205px", "opacity": "1"}, 400, "easeInOutExpo");
		}
	}	
	function twitterbirdOut(){
		$("#dslr").stop(true, false).animate({"top": "-98px"}, 800, "easeInOutExpo");
		$("#iphone").stop(true, false).animate({"top": "-49px"}, 850, "easeInOutExpo");
		$("#twitterbird").stop(true, false).animate({"bottom": "-202px"}, 700, "easeInOutExpo");
		
		if($.browser.msie && $.browser.version < 9){
			$("#zero p#text-twitter").stop(true, false).css({"top": "210px"}).hide();
		} else {
			$("#zero p#text-twitter").stop(true, false).animate({"top": "210px", "opacity": "0", "filter": ""}, 400, "easeInOutExpo", resetTwitter);
		}
	}
	
		function resetTwitter(){
			$("#zero p#text-twitter").hide();
		}
	
	/* #one - Recent work slideshow -------------------------------------------------------------------------- */
	/* ------------------------------------------------------------------------------------------------------- */
	
	// "Slides" SLIDESHOW
	$("#one").slides({
		preload: true,
		//preloadImage: "/gfx/slideshow/loading.gif",
		play: 5000,
	    pause: 7000,
	    slideEasing: "easeInOutExpo",
	    slideSpeed: 600,
	    hoverPause: true
	});
	
	// Slides Caption hover
	$("div.slide").hover(slideOver, slideOut);	
		
	function slideOver(){
		$(".caption").stop(true, false).animate({"bottom":"1px"}, 400, "easeInOutExpo");
	}
		
	function slideOut(){
		$(".caption").stop(true, false).animate({"bottom":"-140px"}, 700, "easeInOutExpo");
	}
	
	/* #two - Dribbble anim ---------------------------------------------------------------------------------- */
	/* ------------------------------------------------------------------------------------------------------- */
	
	$("section#two a#dribbble").hover(mouseOverTwo, mouseOutTwo);	
		
	function mouseOverTwo(){
		$("section#two a#dribbble img#logo-dribbble").stop(true, false).animate({"top": "10px", "padding-bottom": "80px"}, 400, "easeOutBack");
		if($.browser.msie && $.browser.version < 9){
			$("section#two p").show().stop(true, false).css({"top": "145px"});
		} else {
			$("section#two p").show().stop(true, false).animate({"top": "145px", "opacity": "1"}, 400, "easeOutBack");
		} 
		
		$("section#two a#dribbble img#logo-dribbble-shadow").show().stop(true, false).animate({"width": "36px","height": "17px", "left": "164px", "top": "129px","opacity": "1", "filter": ""}, 400, "easeOutBack");
	}
		
	function mouseOutTwo(){
		$("section#two a#dribbble img#logo-dribbble").stop(true, false).animate({"top": "60px", "padding-bottom": "0"}, 500, "easeOutBounce");
		
		if($.browser.msie && $.browser.version < 9){
			$("section#two p").hide().stop(true, false).css({"top": "149px"});
		} else {
			$("section#two p").stop(true, false).animate({"top": "149px", "opacity": "0"}, 400, "easeOutBack", resetTwoP );
		} 
		$("section#two a#dribbble img#logo-dribbble-shadow").show().stop(true, false).animate({"width": "0","height": "0", "left": "182px", "top": "137px", "opacity": "1", "filter": ""}, 400, "easeOutBounce");
	}
		function resetTwoP(){
			$("section#two p").hide();
		}
		
	/* #three - Sketchywill ---------------------------------------------------------------------------------- */
	/* ------------------------------------------------------------------------------------------------------- */	
	
	$("section#three a#sketchywill").hover(mouseOverThree, mouseOutThree);	
		
	function mouseOverThree(){
		var randomNumber1= (Math.floor(Math.random()*61) -30);
		var randomNumber2= (Math.floor(Math.random()*61) -30);
		var randomNumber3= (Math.floor(Math.random()*61) -30);
		$("#three a#sketchywill img#sketchpad1").stop(true, false).animate({"left":"-10px", "top": "5px", "rotate": randomNumber1}, 300, "easeOutBack");
		$("#three a#sketchywill img#sketchpad2").stop(true, false).animate({"left":"110px", "top": "-15px", "rotate": randomNumber2}, 300, "easeOutBack");
		$("#three a#sketchywill img#pen").stop(true, false).animate({"left":"175px", "top": "65px", "rotate": randomNumber3}, 300, "easeOutBack");
	}
		
	function mouseOutThree(){
		$("#three a#sketchywill img#sketchpad1").stop(true, false).animate({"left":"16px", "top": "27px", "rotate": "0"}, 700, "easeOutElastic");
		$("#three a#sketchywill img#sketchpad2").stop(true, false).animate({"left":"90px", "top": "34px", "rotate": "0"}, 700, "easeOutElastic");
		$("#three a#sketchywill img#pen").stop(true, false).animate({"left":"159px", "top": "43px", "rotate": "0"}, 700, "easeOutElastic");		
	}
	
	/* #four - Flickr popup ---------------------------------------------------------------------------------- */
	/* ------------------------------------------------------------------------------------------------------- */
	
	$("section#four div#flickr-container").hover(mouseOverFour, mouseOutFour);	
		
	function mouseOverFour(){
		$("div#popup-flickr").show().stop(true, false).animate({"top": "20px", "opacity": "1"}, 200, "easeOutExpo" );
		$("section#four a#logo-flickr").stop(true, false).animate({"rotate": "180px", "top": "74px", "padding-top": "20px"}, 200, "easeOutExpo");
	}
	
	function mouseOutFour(){
		$("div#popup-flickr").stop(true, false).animate({"top": "48px", "opacity": "0"}, 200, "easeInOutExpo", resetFlickrPopup );
		$("section#four a#logo-flickr").stop(true, false).animate({"rotate": "0", "top": "54px", "padding-top": "0"}, 200, "easeInOutExpo");
	}	
		function resetFlickrPopup(){
			$("div#popup-flickr").hide();
		}
    
    /* #five (Flash) Hover animation ------------------------------------------------------------------------- */
    /* ------------------------------------------------------------------------------------------------------- */
	
	$("#five a").hover(mouseOverFive, mouseOutFive);
	
	function mouseOverFive(){
		if($.browser.msie && $.browser.version < 9){ //just show the poster for poor old IE
			$("img#posterbg").show().stop(true, false).css({"top": "-120px", "height": "400px"});
			$("img#posterframe").show().stop(true, false).css({"top": "-120px", "height": "400px"});
			$("img#blood").show().stop(true, false).css({"top": "-115px",  "height": "217px"});
			$("img#scream").show().stop(true, false).delay(200).css({"top": "20px"});
			$("img#flshadow").show().stop(true, false).delay(200).css({"top": "-110px", "height": "295px"});
			$("#five p").show();
		} else { //set the animation in motion for non-crap browsers
			$("img#posterbg, img#posterframe, img#blood, img#scream, img#flshadow, h5#text-shock, h5#text-horror, h5#text-ripcurl, #five p").css({"opacity": "0", "filter": ""});
			$("img#posterbg").show().stop(true, false).animate({"top": "-120px", "height": "400px", "opacity": "1", "filter": ""}, 200, "easeOutExpo", screamAnim );
			$("img#posterframe").show().stop(true, false).animate({"top": "-120px", "height": "400px", "opacity": "1", "filter": ""}, 2000, "easeOutExpo");
		}
	}
		function screamAnim(){		
			$("img#blood").show().stop(true, false).animate({"top": "-115px",  "height": "217px", "opacity": "1", "filter": ""}, 3000, "easeOutExpo" );
			$("img#scream").show().stop(true, false).delay(500).animate({"top": "20px", "opacity": "1", "filter": ""}, 500, "easeOutElastic" );
			$("img#flshadow").show().stop(true, false).animate({"top": "-110px", "height": "295px", "opacity": "1", "filter": ""}, 7000, "easeInOutExpo");
			$("h5#text-shock").show().stop(true, false).animate({"font-size": "50px", "opacity": "1", "filter": ""}, 200, "easeOutBounce", textHorrorAnim ).delay(2000).animate({"opacity": "0", "filter": ""}, 600, "easeInExpo");
		}			
			function textHorrorAnim(){
				$("h5#text-horror").show().stop(true, false).delay(200).animate({"font-size": "50px", "opacity": "1", "filter": ""}, 200, "easeOutBounce", textRipcurlAnim ).delay(2000).animate({"opacity": "0", "filter": ""}, 600, "easeInExpo");
			}
				function textRipcurlAnim(){
					$("h5#text-ripcurl").show().stop(true, false).delay(200).animate({"font-size": "50px", "opacity": "1", "filter": ""}, 200, "easeOutBounce", textP ).delay(2000).animate({"opacity": "0", "filter": ""}, 600, "easeInExpo");
				}
					function textP(){
						$("#five p").show().stop(true, false).animate({"opacity": "1", "filter": ""}, 2000, "easeOutExpo" );	
					}
												
	function mouseOutFive(){
		if($.browser.msie && $.browser.version < 9){ //just hide the poster for poor old IE
			$("img#posterbg, img#posterframe, img#blood, img#scream, img#flshadow, h5#text-shock, h5#text-horror, h5#text-ripcurl, #five p").stop(true, false).hide();
			reset();
		} else { //fade it out for the good guys
			$("img#posterbg, img#posterframe, img#blood, img#scream, img#flshadow, h5#text-shock, h5#text-horror, h5#text-ripcurl, #five p").stop(true, false).animate({"opacity": "0", "filter": ""}, 300, "easeOutExpo", reset );
		}
		
	}
		function reset(){
			$(this).hide();
			$("img#posterbg").css({"top": "0", "height": "150px"});
			$("img#posterframe").css({"height": "400px"});
			$("img#blood").css({"top": "-125px", "height": "100px"});
			$("img#scream").css({"top": "120px" });
			$("img#flshadow").css({"height": "100px", "top": "180px"});
			$("h5#text-shock, h5#text-horror, h5#text-ripcurl").css({"font-size": "0"});
		}
	
	/* #six - Soundcloud ------------------------------------------------------------------------------------- */
	/* ------------------------------------------------------------------------------------------------------- */	
	
	$("section#six a#logo-soundcloud").hover(mouseOverSix, mouseOutSix);	
		
	function mouseOverSix(){
		$("section#six a#logo-soundcloud").stop(true, false).animate({"top": "30px"}, 400, "easeOutBack");
		
		if($.browser.msie && $.browser.version < 9){
			$("section#six p").show().stop(true, false).css({"top": "96px"});
		} else {
			$("section#six p").show().stop(true, false).animate({"top": "96px", "opacity": "1"}, 400, "easeOutBack");
		} 
	}
		
	function mouseOutSix(){
		$("section#six a#logo-soundcloud").stop(true, false).animate({"top": "50px"}, 400, "easeOutBack");
		
		if($.browser.msie && $.browser.version < 9){
			$("section#six p").hide().stop(true, false).css({"top": "105px"});
		} else {
			$("section#six p").stop(true, false).animate({"top": "105px", "opacity": "0"}, 400, "easeOutBack", resetSixP );
		} 
	}
	
		function resetSixP(){
			$("section#six p").hide();
		}
		
	/* #seven - Cheap Plastic Scenery ------------------------------------------------------------------------ */
	/* ------------------------------------------------------------------------------------------------------- */
	
	$("section#seven a").hover(mouseOverSeven, mouseOutSeven);	
		
	function mouseOverSeven(){
		if(isiPhone() == true){
			$("#seven p").stop(true, false).animate({"top": "20px", "opacity": "0"}, 300, "easeOutExpo" );
			$("#seven img#cps").show().stop(true, false).animate({"top": "30px", "opacity": "1"}, 300, "easeOutExpo" );
		} else if($.browser.msie && $.browser.version < 9){
			$("#seven p").stop(true, false).css({"top": "100px"}).hide();
			$("#seven img#cps").show().stop(true, false).css({"top": "82px"});
		} else {
			$("#seven p").stop(true, false).animate({"top": "100px", "opacity": "0"}, 300, "easeOutExpo" );
			$("#seven img#cps").show().stop(true, false).animate({"top": "82px", "opacity": "1"}, 300, "easeOutExpo" );
		}
		
	}
		
	function mouseOutSeven(){
		if(isiPhone() == true){
			$("#seven p").show().stop(true, false).animate({"top": "50px", "opacity": "1"}, 300, "easeInOutExpo" );
			$("#seven img#cps").stop(true, false).animate({"top": "60px", "opacity": "0"}, 300, "easeInOutExpo", resetCps);
		} else if($.browser.msie && $.browser.version < 9){
			$("#seven p").show().stop(true, false).css({"top": "110px"});
			$("#seven img#cps").stop(true, false).css({"top": "92px"}).hide();
		} else {
			$("#seven p").show().stop(true, false).animate({"top": "110px", "opacity": "1"}, 300, "easeInOutExpo" );
			$("#seven img#cps").stop(true, false).animate({"top": "92px", "opacity": "0"}, 300, "easeInOutExpo", resetCps);
		}
	}	
		function resetCps(){
			$("#seven img#cps").hide();
		}
		
	/* #eight - Free download -------------------------------------------------------------------------------- */
	/* ------------------------------------------------------------------------------------------------------- */
	
	//$("section#eight div, section#eight a, #eight img#slit-shadow, #eight p#fileformat").hover(mouseOverEight, mouseOutEight); //Replaced this as animation would restart every time one of the elements was hovered over	
	$("section#eight").hover(mouseOverEight, mouseOutEight);
		
	function mouseOverEight(){
		if($.browser.msie && $.browser.version < 9){
			$("#eight a").show();
			$("#eight img#slit-shadow, #eight p#fileformat").show();
			$("#eight div#wordblock").hide();
			$("#eight div#patternpreview img").show().stop(true, false).animate({"top": "-421px"}, 5000, "easeInOutExpo");
		} else {
			$("#eight a").show().stop(true, false).animate({"opacity": "1", "filter": ""}, 400, "easeInOutExpo" );
			$("#eight img#slit-shadow, #eight p#fileformat").show().stop(true, false).animate({"opacity": "1", "filter": ""}, 400, "easeInOutExpo" );
			$("#eight div#wordblock").stop(true, false).animate({"opacity": "0", "filter": ""}, 300, "easeInOutExpo", hideWordBlock );
			$("#eight div#patternpreview img").show().stop(true, false).animate({"opacity":"1", "filter": ""},400, "easeInOutExpo" ).animate({"top": "-421px"}, 5000, "easeInOutExpo");
		}		
	}
		function hideWordBlock(){
			$("#eight div#wordblock").hide();
		}
		
	function mouseOutEight(){
		if($.browser.msie && $.browser.version < 9){
			$("#eight a").hide();
			$("#eight img#slit-shadow, #eight p#fileformat").hide();
			$("#eight div#wordblock").show();
			$("#eight div#patternpreview img").stop(true, false).hide().css({"top":"40px"});
		} else {
			$("#eight a").stop(true, false).animate({"opacity": "0", "filter": ""}, 250, "easeInOutExpo");
			$("#eight img#slit-shadow, #eight p#fileformat").stop(true, false).animate({"opacity": "0", "filter": ""}, 300, "easeInOutExpo" );
			$("#eight div#wordblock").show().stop(true, false).animate({"opacity": "1", "filter": ""}, 600, "easeInOutExpo", resetEight);
			$("#eight div#patternpreview img").stop(true, false).animate({"opacity": "0", "filter": ""}, 350, "easeInOutExpo");
		}
	}	
		function resetEight(){
			$("#eight a, #eight img#slit-shadow, #eight p#fileformat").hide();
			$("#eight div#patternpreview img").css({"top":"40px"}).hide();
		}
		
	/* #nine - Forrst ---------------------------------------------------------------------------------------- */
	/* ------------------------------------------------------------------------------------------------------- */	
	
	$("section#nine a#logo-forrst").hover(mouseOverNine, mouseOutNine);	
		
	function mouseOverNine() {
		var randomNumber = (Math.floor(Math.random() * 21) -10);
		
		if (navigator.userAgent.toLowerCase().match('chrome')) { //weird chrome rotation bug
			$("section#nine a#logo-forrst").stop(true, false).animate({"top": "-15px"}, 300, "easeInBack");
		} else {
			$("section#nine a#logo-forrst").stop(true, false).animate({"rotate": randomNumber, "top": "-15px"}, 300, "easeInBack");
		}		
		
		if($.browser.msie && $.browser.version < 9){
			$("section#nine p").show().stop(true, false).css({"top": "97px"});
		} else {
			$("section#nine p").show().stop(true, false).delay(300).animate({"top": "97px", "opacity": "1"}, 400, "easeOutBack");
		}
	}
		
	function mouseOutNine() {
		$("section#nine a#logo-forrst").stop(true, false).animate({"top": "40px", "rotate": "0"}, 700, "easeInElastic");
		if($.browser.msie && $.browser.version < 9){
			$("section#nine p").hide().stop(true, false).css({"top": "110px"});
		} else {
			$("section#nine p").stop(true, false).animate({"top": "110px", "opacity": "0", "filter": ""}, 400, "easeOutBack", resetNineP );
		}
		
	}
		function resetNineP(){
			$("section#nine p").hide();
		}
		

	/* #ten - Lemon anim ------------------------------------------------------------------------------------- */
	/* ------------------------------------------------------------------------------------------------------- */
	
	$("section#ten").hover(mouseOverTen, mouseOutTen);	
		
	function mouseOverTen(){
		$("#ten a#netliferesearch #nlr-will").stop(true, false).animate({"top": "11", "filter": ""}, 400, "easeOutBack");
		$("#ten a#netliferesearch img#nlr-bg").fadeIn(200);
		if($.browser.msie && $.browser.version < 9){
			$("#ten h3").hide();
			$("#ten p").show().stop(true, false).css({"top": "97px"});
		} else {
			$("#ten h3").stop(true, false).animate({"opacity": "0", "filter": ""}, 200, "easeInOutExpo" );
			$("#ten p").show().stop(true, false).animate({"top": "97px", "opacity": "1", "filter": ""}, 400, "easeOutBack");
			
			/*
if(isiPhone() == true){
				$("#ten a#lemon img#lemon-lars").show().stop(true, false).animate({"left": "70px", "opacity": "1", "filter": ""}, 400, "easeOutBack");
			} else {
				$("#ten a#lemon img#lemon-lars").show().stop(true, false).animate({"left": "14px", "opacity": "1", "filter": ""}, 400, "easeOutBack");
			}
*/
		}
	}

	function mouseOutTen(){
		$("#ten a#netliferesearch #nlr-will").stop(true, false).animate({"top": "81px"}, 500, "easeOutBounce");
		$("#ten a#netliferesearch img#nlr-bg").fadeOut(200);
		if($.browser.msie && $.browser.version < 9){
			$("#ten h3").show();
			$("#ten p").css({"top": "110px"}).hide();
		} else {
			$("#ten h3").show().stop(true, false).animate({"opacity": "1", "filter": ""}, 200, "easeInOutExpo" );
			$("#ten p").stop(true, false).animate({"top": "110px", "opacity": "0", "filter": ""}, 400, "easeOutBack", resetTen );
			
			/*
if(isiPhone() == true){
				$("#ten a#lemon img#lemon-lars").stop(true, false).animate({"left": "143px", "opacity": "0", "filter": ""}, 400, "easeOutBack");
			} else {
				$("#ten a#lemon img#lemon-lars").stop(true, false).animate({"left": "87px", "opacity": "0", "filter": ""}, 400, "easeOutBack");
			}
*/
		}
	}
	
	/* #eleven - LinkedIn ------------------------------------------------------------------------------------ */
	/* ------------------------------------------------------------------------------------------------------- */
	
	$("section#eleven a").hover(mouseOverEleven, mouseOutEleven);	
		
	function mouseOverEleven(){
		$("section#eleven a img").hide();
		$("section#eleven a p").show();
	}
		
	function mouseOutEleven(){
		$("section#eleven a img").show();
		$("section#eleven p").hide();
	}
	
	/* #twelve - Email ---------------------------------------------------------------------------------------- */
	/* ------------------------------------------------------------------------------------------------------- */	
	
	$("section#twelve a").hover(mouseOverTwelve, mouseOutTwelve);	
		
	function mouseOverTwelve(){
		var randomNumber= (Math.floor(Math.random()*41) -20);
		$("section#twelve a div img#envelope").stop(true, false).animate({"top": "5px", "rotate": randomNumber}, 400, "easeInBack")
															.animate({"top": "140px"}, 400, "easeInExpo");
		$("section#twelve a img#postbox").stop(true, false).animate({"top": "50px"}, 300, "easeOutBack");
	}
		
	function mouseOutTwelve(){
		$("section#twelve a div img#envelope").stop(true, false).animate({"top": "33px", "rotate": 0}, 700, "easeOutBack");
		$("section#twelve a img#postbox").stop(true, false).delay(700).animate({"top": "151px"}, 300, "easeOutBack");
	}
		function resetTwelveP(){
			$("section#twelve p").hide();
		}
});