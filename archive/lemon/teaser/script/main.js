$(function(){
	





	/* ------------------------------------------------------------------------------------------------------------- */
	/* LETTERING JS Plugin (no need yet... just in case)															 */
	/* ------------------------------------------------------------------------------------------------------------- */
	
	//$("h3").lettering();
	//$(".lettering").lettering();
	//$(".lettering_words").lettering('words').children('span').lettering();
	
	
	
	
	
	
	/* ------------------------------------------------------------------------------------------------------------- */
	/* TWITTER																										 */
	/* ------------------------------------------------------------------------------------------------------------- */
	
	// Click to reveal secret Twitter header area
	var secretTop = false;
	$("#twitter-btn").click(function() {
		// show secret section
		if(secretTop == false){
			$("header#top").stop(true, false).animate({"height": "170px"/* , "background-color":"#9BBDD2" */}, 400, "easeInOutExpo");
			secretTop = true;
			
			$("#twitterbird").oneTime(400, function (){
				$("#twitterbird").animate({"bottom": "50px"},1100, "easeInOutQuad").animate({"bottom": "40px"},1500, "easeInOutQuad");
			});
			$("#twitterbird").everyTime(3000, "float", function (){ 
				//console.log("do it");
				$("#twitterbird").animate({"bottom": "50px"},1500, "easeInOutQuad").animate({"bottom": "40px"},1500, "easeInOutQuad");
			});
			
			$("#twitterbird img#lemonbirdwing").oneTime(400, function (){ 
				$(this).animate({"rotate": "30", "top": "69px", "right":"-6px"},150, "easeInOutQuad").animate({"rotate": "0", "top": "61px", "right":"-9"},250, "easeInOutQuad");
			});
			$("#twitterbird img#lemonbirdwing").everyTime(650, "flap", function (){ 
				$(this).animate({"rotate": "30", "top": "69px", "right":"-6px"},150, "easeInOutQuad").animate({"rotate": "0", "top": "61px", "right":"-9"},500, "easeInOutQuad");
			});
		} 
		// hide secret section
		else {
			$("header#top").stop(true, false).animate({"height": "26px"/* , "background-color":"#655C52" */}, 250, "easeInOutExpo");
			secretTop = false;
			//console.log("stop it");
			
			$("#twitterbird").stopTime("float");
			$("#twitterbird img#lemonbirdwing").stopTime("flap");
		}
	});
	
		/* Pull in Tweets ---------------------------- */
		
		$('#tweets').tweetable({
			username: 'lemonagency',
			limit: 1, //number of tweets to show
			time: false, //display date
			replies: false //filter out @replys
	 	});
	
	
	
	
	/* ------------------------------------------------------------------------------------------------------------- */
	/* Lemon Logo / Vespa / H1 / H2 click behaviour																	 */
	/* ------------------------------------------------------------------------------------------------------------- */
	
	var logoVisible = true;
	
	$("#lemon-logo, #vespa").click(function() {
		if(logoVisible == true){
			// hide:
			$("h1").stop(true, false).animate({"opacity": "0", "filter":"0"}, 1000, "easeOutExpo");
			$("#lemon-logo").stop(true, false).animate({"opacity": "0", "filter":"0"}, 200, "easeOutExpo");
			// show:
			$("h2").show().stop(true, false).animate({"opacity": "1", "filter":"0"}, 1000, "easeOutExpo");
			$("#vespa").show().stop(true, false).animate({"opacity": "1", "filter":"0", "top":"60px", "right":"120px"}, 600, "easeOutExpo");
			logoVisible = false;
		} else {
			// show:
			$("h1").show().stop(true, false).animate({"opacity": "1", "filter":"0"}, 1000, "easeInExpo");
			$("#lemon-logo").stop(true, false).animate({"opacity": "1", "filter":"0"}, 900, "easeInExpo");
			// hide:
			$("h2").stop(true, false).animate({"opacity": "0", "filter":"0"}, 900, "easeOutExpo");
			$("#vespa").show().stop(true, false).animate({"opacity": "0", "filter":"0", "top":"90px", "right":"60px"}, 600, "easeInBack", resetHeader);
		}
	});
	
	function resetHeader(){
		$("h2").hide();
		$("#vespa").hide().css({"top":"40px", "right":"180px"});
		logoVisible = true;
	}
	
	
	/* ------------------------------------------------------------------------------------------------------------- */
	/* MAIN NAV HOVERS																								 */
	/* ------------------------------------------------------------------------------------------------------------- */
	
		/* Nav hover anim WORK															*/
		/* ---------------------------------------------------------------------------- */
		
		$("li#nav-work a").hover(mouseOverWork, mouseOutWork);
		
		function mouseOverWork(){
			var randomNumberRotate= (Math.floor(Math.random()*50) -65);
			var randomNumberHeight= (Math.floor(Math.random()*30) + 155);
			$("li#nav-work a img#choppingboard").stop(true, false).animate({"top": "155px"}, 200, "easeInOutExpo");
			$("li#nav-work a img#knife").stop(true, false).animate({"top": randomNumberHeight, "rotate": randomNumberRotate }, 300, "easeOutBack");
		}
		
		function mouseOutWork(){
			$("li#nav-work a img#choppingboard").stop(true, false).animate({"top": "163px"}, 300, "easeInOutExpo");
			$("li#nav-work a img#knife").stop(true, false).animate({"top": "229px", "rotate": 0}, 200, "easeInOutExpo");
		}
		
		/* Nav hover anim CLIENTS														*/
		/* ---------------------------------------------------------------------------- */
		
		// Set up leaves
		function randomLeafPositionGenerate(){
			return (Math.floor(Math.random()*130) + 60);
		}
		
		function randomLeafRotationGenerate(){
			return (Math.floor(Math.random()*90) -45);
		}
		
		function resetLeaves(){
			$("li#nav-clients a img#leaf1").css({"top": "170px", "left": randomLeafPositionGenerate(), "opacity": "1", "rotate": "0"});
			$("li#nav-clients a img#leaf2").css({"top": "170px", "left": randomLeafPositionGenerate(), "opacity": "1", "rotate": "0"});
		}
		
		$("li#nav-clients a img#leaf1").css({"left": randomLeafPositionGenerate() });
		$("li#nav-clients a img#leaf2").css({"left": randomLeafPositionGenerate() });
		
		$("li#nav-clients a").hover(mouseOverClients, mouseOutClients);
		
		function mouseOverClients(){			
			$("li#nav-clients a img#tree").stop(true, false).animate({"rotate": "-5" }, 40, "easeOutBack")
															.animate({"rotate": "5" }, 90, "easeOutBack")
															.animate({"rotate": "-5" }, 40, "easeOutBack")
															.animate({"rotate": "5" }, 90, "easeOutBack")
															.animate({"rotate": "0" }, 150, "easeOutBack");
			
			$("li#nav-clients a img#leaf1").stop(true, false).animate({"top": "260px", "rotate": randomLeafRotationGenerate(), "opacity": "0" }, 1400, "easeOutExpo");
			$("li#nav-clients a img#leaf2").stop(true, false).animate({"top": "260px", "rotate": randomLeafRotationGenerate(), "opacity": "0" }, 2400, "easeOutExpo", resetLeaves);
		}
			
		function mouseOutClients(){
			$("li#nav-clients a img#leaf1").stop(true, false).animate({"top": "170px", "left": randomLeafPositionGenerate(), "opacity": "1", "rotate": "0"}, 0);
			$("li#nav-clients a img#leaf2").stop(true, false).animate({"top": "170px", "left": randomLeafPositionGenerate(), "opacity": "1", "rotate": "0"}, 0);
		}
		
		/* Nav hover anim ABOUT															*/
		/* ---------------------------------------------------------------------------- */
		
		$("li#nav-about a").hover(mouseOverAbout, mouseOutAbout);	
			
		function mouseOverAbout(){
			$("li#nav-about a img#lemon-will").stop(true, false).animate({"top": "165px"}, 200, "easeOutBack");
			$("li#nav-about a img#lemon-knut").show().stop(true, false).animate({"left": "48px", "opacity": "1"}, 250, "easeOutBack");
			$("li#nav-about a img#lemon-geir").show().stop(true, false).animate({"left": "126px", "opacity": "1"}, 300, "easeOutBack");
			$("li#nav-about a img#lemon-arnie").show().stop(true, false).animate({"left": "160px", "opacity": "1"}, 350, "easeOutBack");
			$("li#nav-about a img#lemon-lars").show().stop(true, false).animate({"left": "14px", "opacity": "1"}, 400, "easeOutBack");
		}
			
		function mouseOutAbout(){
			$("li#nav-about a img#lemon-will").stop(true, false).animate({"top": "181px"}, 500, "easeOutBounce");
			$("li#nav-about a img#lemon-lars").stop(true, false).animate({"left": "87px", "opacity": "0"}, 400, "easeOutBack");
			$("li#nav-about a img#lemon-knut").stop(true, false).animate({"left": "87px", "opacity": "0"}, 400, "easeOutBack");
			$("li#nav-about a img#lemon-geir").stop(true, false).animate({"left": "87px", "opacity": "0"}, 400, "easeOutBack");
			$("li#nav-about a img#lemon-arnie").stop(true, false).animate({"left": "87px", "opacity": "0"}, 400, "easeOutBack", resetAbout);
		}
			function resetAbout(){
				$("li#nav-about a img#lemon-lars, li#nav-about a img#lemon-knut, li#nav-about a img#lemon-geir, li#nav-about a img#lemon-arnie").hide();		
			}
		
		/* Nav hover anim CONTACT														*/
		/* ---------------------------------------------------------------------------- */
		
		$("li#nav-contact a").hover(mouseOverContact, mouseOutContact);
		
		function mouseOverContact(){
			$("li#nav-contact a img#phone").stop(true, false).animate({"left": "44px" }, 150, "easeInOutExpo");
			$("li#nav-contact a img#receiver").stop(true, false).animate({"top": "100px", "left": "164px"}, 150, "easeInOutExpo");
		}
		
		function mouseOutContact(){
			$("li#nav-contact a img#phone").stop(true, false).animate({"left": "64px" }, 150, "easeInOutExpo");
			$("li#nav-contact a img#receiver").stop(true, false).animate({"top": "127px", "left": "134px"}, 150, "easeInOutExpo");
		}
	
	
	
	
	
	
	/* ------------------------------------------------------------------------------------------------------------- */
	/* WORK - Project Slideshows																					 */
	/* ------------------------------------------------------------------------------------------------------------- */
	
	/* Main content slideshow (Top level)											*/
	/* ---------------------------------------------------------------------------- */
	
	$("section#work div.content").slides({
		preload: true,
		preloadImage: "gfx/slideshow/loading.gif",
		play: 0,
	    pause: 5000,
	    slideEasing: "easeInOutExpo",
	    slideSpeed: 600,
	    bigTarget: false
	});
	
	/* Image detail slideshow (Nested, per project)									*/
	/* ---------------------------------------------------------------------------- */
	
	$(".detail-slideshow").cycle({
		delay: 2000,
		speed: 500
		/*
		after:         null,   // transition callback (scope set to element that was shown) 
	    before:        null,   // transition callback (scope set to element to be shown) 
	    delay:         0,      // additional delay (in ms) for first transition (hint: can be negative) 
	    fit:           0,      // force slides to fit container 
	    fx:           'fade',  // name of transition function 
	    height:       'auto',  // container height 
	    metaAttr:     'cycle', // data- attribute that holds the option data for the slideshow 
	    next:          null,   // id of element to use as click trigger for next slide 
	    pause:         0,      // true to enable "pause on hover" 
	    prev:          null,   // id of element to use as click trigger for previous slide 
	    timeout:       4000,   // milliseconds between slide transitions (0 to disable auto advance) 
	    speed:         1000,   // speed of the transition (any valid fx speed value) 
	    slideExpr:     null,   // expression for selecting slides (if something other than all children is required) 
	    sync:          1,      // true if in/out transitions should occur simultaneously 
	 
	    // the following options let you create transitions other than fade 
	    cssBefore:     {},     // properties that define the initial state of the slide before transitioning in 
	    cssAfter:      {},     // properties that defined the state of the slide after transitioning out 
	    animIn:        {},     // properties that define how the slide animates in 
	    animOut:       {}      // properties that define how the slide animates out 
		*/
	});
		
	/* Thumbnail Carousel (1 row)													*/
	/* ---------------------------------------------------------------------------- */
	
	$("#projectthumbs").jCarouselLite({
        btnNext: ".next-thumbset",
        btnPrev: ".prev-thumbset",
        scroll: 1,
        visible: 4,
        easing: "easeInOutExpo",
        speed: 400,
        circular: false
    });
	$("#projectthumbs").hide(); 
	
	
		/* Work toggle btn behaviour (Toggle between main slideshow & thumbs)			*/
		/* ---------------------------------------------------------------------------- */
		
		var workToggle = "singleProject";
		$("div#work-toggle-btn button#work-full-btn").addClass("work-toggle-on");
		
		$("div#work-toggle-btn button#work-full-btn").click(function() {
			if(workToggle != "singleProject"){
				showSingleProject();
				workToggle = "singleProject";
			}
		});
		
		$("div#work-toggle-btn button#work-thumbs-btn").click(function() {
			if(workToggle == "singleProject"){
				showProjectThumbgrid();
			}
		});
		
		function showSingleProject(){
			// show single project
			$("div#projectthumbs, section#work div.content button.prev-thumbset, section#work div.content button.next-thumbset").fadeOut();
			$("section#work div.content div.slides_container, section#work div.content div.prev, section#work div.content div.next").fadeIn();
			
			// shift background back up vertically
			$("section#work").stop(true, false).animate({"height": "658px"}, 300, "easeInOutExpo");
			$("section#work div.content").stop(true, false).animate({"height": "420px"}, 300, "easeInOutExpo");
			$("section#work div.content nav#categories-nav").fadeOut(400);
			
			// change variable + classes
			$("div#work-toggle-btn button#work-full-btn").addClass("work-toggle-on");
			$("div#work-toggle-btn button#work-thumbs-btn").removeClass("work-toggle-on");
		}
		
		function showProjectThumbgrid(){
			// show project thumbgrid
			$("div#projectthumbs").fadeIn().css({"width":"850px", "height":"120px"});
			$("section#work div.content button.prev-thumbset, section#work div.content button.next-thumbset").fadeIn();
			$("section#work div.content div.slides_container, section#work div.content div.prev, section#work div.content div.next").fadeOut();
			
			// shift background downwards vertically
			$("section#work").stop(true, false).animate({"height": "698px"}, 300, "easeInOutExpo");
			$("section#work div.content").stop(true, false).animate({"height": "460px"}, 300, "easeInOutExpo");
			$("section#work div.content nav#categories-nav").fadeIn(400);
			
			// change variable + classes
			$("div#work-toggle-btn button#work-thumbs-btn").addClass("work-toggle-on");
			$("div#work-toggle-btn button#work-full-btn").removeClass("work-toggle-on");
			workToggle = "thumbGrid";
			
			// make the Gentlemon hover then fall and bounce
			$("#gentlemon").stop(true, false).animate({"bottom":"0px", "height":"152px"}, 300, "easeInOutExpo")
											 .animate({"bottom":"-50px", "height":"132px"}, 400, "easeOutBounce");
		}
		
		
		/* Work project thumbs hover													*/
		/* ---------------------------------------------------------------------------- */
		
		$("div#projectthumbs ul li div").hover(mouseOverThumb, mouseOutThumb);
		
		function mouseOverThumb(){
			$(this).children("h6, p").stop(true, false).animate({"opacity": "0"}, 200);
			$(this).children("aside.categories").stop(true, false).animate({"bottom": "-20px"}, 150, "easeInQuad");
		}
		
		function mouseOutThumb(){
			$(this).children("h6, p").stop(true, false).animate({"opacity": "1"}, 200);
			$(this).children("aside.categories").stop(true, false).animate({"bottom": "5px"}, 150, "easeOutQuad");
		}
		
		/* Work project thumbs click													*/
		/* ---------------------------------------------------------------------------- */
		
		$("div#projectthumbs ul li div").click(function() {
			showSingleProject();
		});
		
		/* "Lean" London Gentlemon when clicking right / left on the gallery				*/
		/* ---------------------------------------------------------------------------- */
		
		$("div.prev, button.prev-thumbset").click(function() {
			$("#gentlemon").animate({"rotate": "10"}, 250, "easeInExpo").animate({"rotate": "0"}, 250, "easeOutExpo");
		});
		
		$("div.next, button.next-thumbset").click(function() {
			$("#gentlemon").animate({"rotate": "-10"}, 250, "easeInExpo").animate({"rotate": "0"}, 250, "easeOutExpo");
		});
		

		/* London Gentlemon squeezy jump anim (add random text sayings)					*/
		/* ---------------------------------------------------------------------------- */

		$("#gentlemon").click(function() {
			var randomNumber = (Math.floor(Math.random()*90) -50);
		
			$(this).stop(true, false).animate({"height":"100px"}, 200, "easeInOutExpo")
									 .animate({"bottom":randomNumber, "height":"152px"}, 300, "easeInOutExpo")
									 .animate({"bottom":"-50px", "height":"132px"}, 300, "easeOutBounce");
		});
		
	/* ------------------------------------------------------------------------------------------------------------- */
	/* CLIENTS																										 */
	/* ------------------------------------------------------------------------------------------------------------- */		
		
		/* Client pop-up menu on hover													*/
		/* ---------------------------------------------------------------------------- */
		
		$("li.client").hover(mouseOverClient, mouseOutClient);
		
		function mouseOverClient(){
			$(this).children("div.client-popup").css({"display":"block"});
			
			// Calculate vertical position of pop-up bubble based on height ( therefore number of items in list)
			var popupHeight = $(this).find("ul").height();
			var calculatedTop = -(popupHeight / 2) + 11;
			$(this).find("ul").css({"top":calculatedTop});
		}
		
		function mouseOutClient(){
			$(this).children("div.client-popup").fadeOut();
		}
		
		// Highlight client website link when hovering just on the client name
		$("li.client a").hover(mouseOverClientLink, mouseOutClientLink);
		function mouseOverClientLink(){
			$(".client-site a").css({"color":"#554E31"});
		}
		function mouseOutClientLink(){
			$(".client-site a").css({"color":"#96938F"});
		}
		
	/* ------------------------------------------------------------------------------------------------------------- */
	/* ABOUT - SUB NAVIGATION (EMPLOYEES, THE COMPANY, WORKING HERE)												 */
	/* ------------------------------------------------------------------------------------------------------------- */	

	// On page load
	$(".tab-content").hide(); //Hide all content
	$("nav#aboutnav ul li:first").addClass("active").show(); //Activate first tab
	$(".tab-content:first").show(); //Show first tab content

	// On nav item click
	$("nav#aboutnav ul li").click(function() {

		$("nav#aboutnav ul li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab-content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		
		// Change background according to active tab
		//alert(activeTab);
		/*
		if(activeTab == "#employees"){
			$("section#about div.content").css({"background": 'url("../gfx/bg-herringbone-repeat.png")'});
		} else if (activeTab == "#thecompany"){
			$("section#about div.content").css({"background": 'url("../gfx/bg-diagstripe-repeat.png")'});
		} else {
		
		}
		*/
		
		return false;
	});
	
	/* ABOUT > EMPLOYEES															*/
	/* ---------------------------------------------------------------------------- */
	
	// Phone number show / hide	
	$(".btn-phone").click(function() {
		// hide all others
		$("#employees .employee div.btn-phone").stop(true, false).animate({"right": "88px"}, 300, "easeInOutExpo");
		$("#employees .employee a").stop(true, false).animate({"left": "89px"}, 300, "easeInOutExpo");
		$("#employees .employee p").hide(300);
		
		// show selected number
		$(this).stop(true, false).animate({"right": "38px"}, 300, "easeInOutExpo");
		$(this).parent().find("a").stop(true, false).animate({"left": "35px"}, 300, "easeInOutExpo");
		$(this).parent().find("p").show(300);
	});
	
	// Squeezy employee lemon jump
	$("li.employee img").click(function() {
		var randomNumber = (Math.floor(Math.random()*40) +140);
		var lemonWidth = $(this).width();
		var lemonHeight = $(this).height();
		var lemonSqueezed = lemonHeight - 20;
		var lemonStretched = lemonHeight + 30;
		$(this).css({"width": lemonWidth});
		
		if(lemonHeight >= 90){ //stop lemons from shrinking indefinitely from quick repeated clicking
			$(this).stop(true, false).animate({"height": lemonSqueezed}, 200, "easeInOutExpo")
									 .animate({"bottom":randomNumber, "height": lemonStretched}, 300, "easeInOutExpo")
									 .animate({"bottom":"130px", "height": lemonHeight}, 300, "easeOutBounce");
		}
	});
	
	
	
	/* ABOUT > WORKING HERE Photo slideshow											*/
	/* ---------------------------------------------------------------------------- */
	
	$("#workinghere").slides({
		preload: true,
		preloadImage: "gfx/slideshow/loading.gif",
		play: 5000,
	    pause: 7000,
	    slideEasing: "easeInOutExpo",
	    slideSpeed: 600,
	    hoverPause: true,
	    bigTarget: false
	});
	
		// Caption Hover
		$("div#workinghere.tab-content div.slides_container div.slides_control div.slide, .prev, .next").hover(slideOver, slideOut);
		
		function slideOver(){
			$(".caption").stop(true, false).animate({"bottom":"0"}, 400, "easeInOutExpo");
		}
			
		function slideOut(){
			$(".caption").stop(true, false).animate({"bottom":"-140px"}, 400, "easeInOutExpo");
		}
	
	
	

	
	/* ------------------------------------------------------------------------------------------------------------- */
	/* PARALLAX SHIZ																				 				 */
	/* ------------------------------------------------------------------------------------------------------------- */
	
	/* Parallax on Mouse move														*/
	/* ---------------------------------------------------------------------------- */
	
	/*
	$('#cloud1').plaxify({"xRange":120,"yRange":60,"invert":true})
	$('#cloud2').plaxify({"xRange":40,"yRange":20,"invert":true})
	$('#cloud3').plaxify({"xRange":30,"yRange":10,"invert":true})
	$('#cloud4').plaxify({"xRange":20,"yRange":10,"invert":true})
	$.plax.enable()
	*/
	
	$("#vikingarrow1").plaxify({"xRange":120,"yRange":60,"invert":true});
	$("#vikingarrow2").plaxify({"xRange":40,"yRange":20,"invert":true});
	$("#vikingarrow3").plaxify({"xRange":30,"yRange":10,"invert":true});
	$.plax.enable();
	
	/* Parallax on page scroll														*/
	/* ---------------------------------------------------------------------------- */
	
	// Move sky bg - very jerky
	/*
	$('#contact').scrollParallax({
		'speed': -1.5
	});
	*/
	
	$("#cloud1").scrollParallax({ "speed": -1.7 });
	$("#cloud2").scrollParallax({ "speed": -1.3 });
	$("#cloud3").scrollParallax({ "speed": -1.2 });
	$("#cloud4").scrollParallax({ "speed": -1.3 });
	
	/* ------------------------------------------------------------------------------------------------------------- */
	/* VIKING MISC																									 */
	/* ------------------------------------------------------------------------------------------------------------- */
	
	/*
	$("#vikingattacker").everyTime ( 4610, function (){ 
		$(this).delay(4000).animate({"top": "955px"},200, "easeOutBack").animate({"top": "972px"},400, "easeOutBounce");
	});
	*/

	/*
	$("#vikingattacker2").everyTime ( 6610, function (){ 
		$(this).delay(6000).animate({"top": "960px"},200, "easeOutBack").animate({"top": "972px"},400, "easeOutBounce");
	});
	*/
	
	
	/* EXPERIMENTAL PARALLAX ---------------------------------------------------------------------------------------- */
	/*  ------------------------------------------------------------------------------------------------------------- */
	
	/*
	$('#hills1').scrollParallax({
		'speed': -1.1
	});
	
	$('#hills2').scrollParallax({
		'speed': -1.1
	});
	*/
	
	/* Parallax sky background behind slottsfjell tower  */	
		//$("section#contact").stop().animate({backgroundPosition: "0 bottom"}, 300, "easeInOutExpo" );
		//$("section#contact").scrollingParallax('gfx/footer_blue_repeat3.jpg');
		
	/*
		$('section#contact').scroll(function(){
			var x = $(this).scrollTop();
			$(this).css('backgroundPosition','0% '+parseInt(-x/10)+'px');
		});
	*/
	
	/*
		$(window).scroll(function(){
    		$("section#contact").css(backgroundPosition,"0 400px");
		})
	*/
	
	/* RANDOM rotation of employee "cards" for capable browsers (looks terrible in non-Mac/webkit. Hmm...) ---------- */
	/*  ------------------------------------------------------------------------------------------------------------- */
	
	/*
	$("li.employee").each(function(i){
		var randomNumber= (Math.floor(Math.random()*8) -4);
		$(this).css({"rotate": randomNumber});
	});
	
	$("li.employee").hover(mouseOverEmployee, mouseOutEmployee);
	
	function mouseOverEmployee(){
		$(this).stop(true, false).animate({"rotate": "0"}, 100, "easeOutExpo");
	}
	
	function mouseOutEmployee(){
		var randomNumber= (Math.floor(Math.random()*8) -4);
		$(this).stop(true, false).animate({"rotate": randomNumber}, 100, "easeOutExpo");
	}*/
});