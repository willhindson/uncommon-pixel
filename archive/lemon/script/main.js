$(function(){

	// Load Typekit
	/* try{Typekit.load();} catch(e) {} */

	// Re-enable css active pseudo-styles in iOS
	// document.addEventListener("touchstart", function() {},false);

	// Prevent Images from being dragged!
	$('img').bind('dragstart', function(event) { event.preventDefault(); });



	/* ------------------------------------------------------------------------------------------------------------ */
	/* Page smoothscroll, scroll position detection & highlighting for dropdown nav									*/
	/* ------------------------------------------------------------------------------------------------------------ */

	function scrollableElement(els) {
		var i, el, isScrollable;
		for (i = 0, argLength = arguments.length; i < argLength; i++) {
			el = arguments[i], scrollElement = $(el);
			if (scrollElement.scrollTop() > 0) {
				return el;
			} else {
				scrollElement.scrollTop(1);
				isScrollable = scrollElement.scrollTop() > 0;
				scrollElement.scrollTop(0);
				if (isScrollable) {
					return el;
				}
			}
		}
		return [];
	}

	var locationPath = filterPath(location.pathname);
	var scrollElem = scrollableElement('html', 'body');

	// Smoothscroll
	function filterPath(string) {
	    return string.replace(/^\// , '').replace(/(index|default).[a-zA-Z]{3,4}$/, '').replace(/\/$/, '');
	}

	/* ------------------------------------------------------------------------------------------------------------ */
	/* Asual deep linking								                     										*/
	/* ------------------------------------------------------------------------------------------------------------ */

	function navigateToSection(sectionName){

		var $target = $("#"+sectionName);
		/*alert($target);*/
		var targetOffset = 0;
		if ($target.offset() != null) {
			targetOffset = $target.offset().top;

			// Animate to target
			if (sectionName == "contact") {
				// Scroll all the way to bottom if contact is clicked on
				//alert("doing it"+scrollElem);
				$(scrollElem).animate({scrollTop: $(document).height() + "px"}, 1000, "easeInOutExpo", function() {
					// Set hash in URL after animation successful
					//location.hash = target;
					updateDropNavYPos();
				});
			} else {

				$(scrollElem).animate({scrollTop: targetOffset - 80}, 600, "easeInOutExpo", function() {
					// Set hash in URL after animation successful
					// location.hash = target;
					updateDropNavYPos();
				});
			}
		}

	}


	var firstLoad = true;
	$.address.externalChange(function(e) {
		// do something depending on the event.value property, e.g.
		//alert(event.value);
		var path = e.value.split("/");
		if(path[1]){
			if(firstLoad){
				$(window).load(function() {
					navigateToSection(path[1]);
				});
			}else {
				//console.log("externalChange "+ e.value);
				navigateToSection(path[1]);
			}
		}

		firstLoad = false;

		//alert("externalChange");
		//$("#nav-contact a").click();
	});






	$('nav#dropnav ul li a[href*=#], nav#headernav ul li a[href*=#]').click( function(event) {
		var target = this.hash.substring(1);

		$.address.value("/"+target);
		navigateToSection(target);
		event.preventDefault();

	});




	// Any links with hash tags in them (can't do ^= because of fully qualified URL potential)
	/*
	$('nav#dropnav ul li a[href*=#], nav#headernav ul li a[href*=#]').each( function() {
	    // Ensure it's a same-page link
		var thisPath = filterPath(this.pathname) || locationPath;
	    if (locationPath == thisPath && (location.hostname == this.hostname || !this.hostname) && this.hash.replace(/#/, '')) {
	    	// Ensure target exists
	    	var $target = $(this.hash)
	    	target = this.hash;
	    	console.log($target);
	    	console.log(target);
	    	if (target) {
	    		// Find location of target
	    		var targetOffset = 0;

	    		$(this).click(function(event) {
					if ($target.offset() != null) {
	    				targetOffset = $target.offset().top;
	    				// Prevent jump-down before the animation (disable the default event of the element, in this case <a>)
	    				event.preventDefault();
	    				// Animate to target

	    				//console.log(target);
	    				if (target == "#contact") {
	    					// Scroll all the way to bottom if contact is clicked on
	    					$('html,body').animate({scrollTop: $(document).height() + "px"}, 1000, "easeInOutExpo", function() {
								// Set hash in URL after animation successful
								//location.hash = target;
								updateDropNavYPos();
	    					});
	    				} else {
	    					$(scrollElem).animate({scrollTop: targetOffset - 80}, 600, "easeInOutExpo", function() {
								// Set hash in URL after animation successful
								// location.hash = target;
								updateDropNavYPos();
	    					});
	    				}
	    			}
	    		});
	    	}
	    }
	});
	*/

	// Scroll to top link

	/*$("a[href='#top']").click(function() {
  		$("html, body").animate({ scrollTop: 0 }, 600, "easeInOutExpo", updateDropNavYPos);
  		return false;
	});
     */

	// Detect scroll down, activate dropdown nav / highlight current sections
	var currentOn = null;

	// Change 'on' state of selected navigation item
	function changeOn($element) {
		if (currentOn != null) {
			currentOn.removeClass("on");
		}
		currentOn = $element;
		currentOn.addClass("on");
	}

		$("nav#dropnav ul li a").click( function() {
			changeOn($(this));
		});

	var docViewTop;
	var dropNav = false;

	function checkPosition() {
		if ($(scrollElem).is(':animated')) {
			// stop the function here if currently animating
			return;
		}
		var docViewTop = $(window).scrollTop();
		var docViewCenter = docViewTop + ($(window).height()/2);

		$("nav#dropnav ul#dropnavSections li a").each( function() {
			var href = $(this).attr("href");
			var elementTop = $(href).offset().top;
			if(docViewCenter > elementTop && docViewCenter < (elementTop+ $(href).height()+100)) {
				changeOn($(this));
			}
		});

		// Check for scroll position as user scrolls, bring in or send out dropdown nav in accordance
		updateDropNavYPos();
	}

	$(window).scroll(checkPosition);

	// Function to be called on page load, when the top link is clicked and any time the dropnav y position needs changing
	function updateDropNavYPos(){
		if ($(window).scrollTop() > 560) {
			$("nav#dropnav").stop(true, false).animate({"top": "-150px"}, 200, "easeInOutExpo");
			$("#social-btn-mask").css({"height":"95px"});
			$("#social-btn").stop(true, false).animate({"top": "-75px", "left":"2px"}, 200, "easeOutBack");
			dropNav = true;
		} else {
			$("nav#dropnav").stop(true, false).delay(600).animate({"top": "-182px"}, 200, "easeInExpo");
			$("#social-btn-mask").css({"height":"170px"});
			$("#social-btn").stop(true, false).animate({"top": "-40px", "left":"10px"}, 200, "easeOutBack");
			dropNav = false;
		}

		if (dropSocial){
			hideSocialDropdown();
			$("#social-btn").removeClass("on");
		}
	}

	// Check once on pageload if dropdown nav needs to be on (eg: if user accesses a section directly from the address bar)
	updateDropNavYPos();




	/* ------------------------------------------------------------------------------------------------------------- */
	/* 0) HEADER - SOCIAL DROPDOWN																					 */
	/* ------------------------------------------------------------------------------------------------------------- */

	var dropSocial = false;

	// Social "News tag" button hover behaviour:
	$("#social-btn").hover(socialBtnOver, socialBtnOut);
	function socialBtnOver(){
		$("#social-btn-mask").css({"height":"170px"});
		if(!dropSocial){
			// If the social dropdown is hidden
			$("#social-btn").stop(true, false).animate({"top":"-10px", "left":"10px"}, 200, "easeOutBack");
		} else {
			// If the social dropdown is showing
			$("#social-btn").stop(true, false).animate({"top":"-60px", "left":"10px"}, 200, "easeOutBack");
		}
	}
	function socialBtnOut(){

		if(dropNav || dropSocial){
			$("#social-btn").stop(true, false).animate({"top": "-75px", "left":"2px"}, 200, "easeOutBack", socialBtnMaskOut );
		} else {
			// If the dropnav is hidden (ie scrolled to top)
			$("#social-btn").stop(true, false).animate({"top":"-40px", "left":"10px"}, 700, "easeOutExpo");
		}
	}

		function socialBtnMaskOut(){
			$("#social-btn-mask").css({"height":"95px"});
		}

	// Click the "News" tag to reveal and hide "secret" Social header area
	$("#social-btn").click(function() {
		if(!dropSocial){
			// Show social dropdown if hidden
			$("#dropnav").stop(true, false).animate({"top": "0"}, 400, "easeInOutExpo");
			$("#social-btn").stop(true, false).animate({"top": "-75px", "left":"2px"}, 200, "easeOutBack");
			$("#social-btn").addClass("on");
			dropSocial = true;

			// Start twitterbird animation
			$("#twitterbird").oneTime(400, function (){
				$("#twitterbird").animate({"bottom": "40px"},1100, "easeInOutQuad").animate({"bottom": "30px"},1500, "easeInOutQuad");
			});
			$("#twitterbird").everyTime(3000, "float", function (){
				$("#twitterbird").animate({"bottom": "40px"},1500, "easeInOutQuad").animate({"bottom": "30px"},1500, "easeInOutQuad");
			});

			if(!($.browser.msie && $.browser.version < 9)){
				$("#lemonbirdwing").oneTime(400, function (){
					$(this).animate({"rotate": "30", "top": "67px", "right":"-6px"},150, "easeInOutQuad").animate({"rotate": "0", "top": "59px", "right":"-9px"},250, "easeInOutQuad");
				});
				$("#lemonbirdwing").everyTime(650, "flap", function (){
					$(this).animate({"rotate": "30", "top": "67px", "right":"-6px"},150, "easeInOutQuad").animate({"rotate": "0", "top": "59px", "right":"-9px"},500, "easeInOutQuad");
				});
			}


			// Drop stack of newspapers in
			$("#social-fb").stop(true, false).delay(600).animate({"top":"105px"}, 360, "easeOutBounce");
		} else {
			// Hide social dropdown
			updateDropNavYPos();
		}
	});

	// Hide secret social header + Stop twitterbird animation:
	function hideSocialDropdown() {
		dropSocial = false;
		$("#twitterbird").stopTime("float");
		$("#lemonbirdwing").stopTime("flap");
		$("#social-btn").removeClass("on");
		$("#social-fb").stop(true, false).delay(800).animate({"top":"-70px"}, 350);
	}

	// When social dropdown is showing and user clicks anywhere else on screen, dropdown hides:
	var mouseOverActiveElement = false;
	$("#dropnav").live("mouseenter", function(){
	    mouseOverActiveElement = true;
	}).live("mouseleave", function(){
	    mouseOverActiveElement = false;
	});

		$("html").click(function(){
		   	if (!mouseOverActiveElement) {
				//console.log('clicked outside active element');
				updateDropNavYPos();
		    }
		});

	// Facebook newspaper stack button behaviour:
	$("#social-fb, #facebook-text, #social-newspaper-guy").hover(mouseOverFb, mouseOutFb);
	function mouseOverFb(){
		$("#social-fb").stop(true, false).animate({"top":"95px"}, 200, "easeOutQuad");
		$("#facebook-text").css({"color":"#3B5998"});
	}
	function mouseOutFb(){
		$("#social-fb").stop(true, false).animate({"top":"105px"}, 200, "easeOutBounce");
		$("#facebook-text").css({"color":"inherit"});
	}

	// Lemon egg twitter nest button behaviour (link to Lemon twitter page):
	var eggReady = true;
	$("#twitter-link, #twitter-text").hover(mouseOverTwitterNest, mouseOutTwitterNest);

	function mouseOverTwitterNest(){
		if(eggReady){
			eggReady = false;
			var randomEggAnim = (Math.floor(Math.random()*3));
			switch (randomEggAnim) {
				case 0:
				  $("#twitter-egg").stop(true, false).effect("shake", { times:4, distance:2, direction: "right" }, 60, resetEgg);
				  break;
				case 1:
				  $("#twitter-egg").stop(true, false).animate({"top":"27px"}, 200).delay(600).animate({"top":"18px"}, 1000, resetEgg);
				  break;
				case 2:
				  $("#twitter-egg").stop(true, false).animate({"top":"8px"}, 200).delay(600).animate({"top":"18px"}, 300, "easeOutBounce", resetEgg);
				  break;
				default:
				$("#twitter-egg").stop(true, false).animate({"top":"27px"}, 200).delay(600).animate({"top":"18px"}, 1000, resetEgg);
			}
		}
		$("#twitter-text").css({"color":"#6699FF"});
	}

	function mouseOutTwitterNest(){
		$("#twitter-text").css({"color":"inherit"});
	}

		function resetEgg(){
			eggReady = true;
		}

	// Pull in Tweets
	/*
	$('#tweets').tweetable({
		username: 'lemonagency',
		limit: 1, //number of tweets to show
		time: false, //display date
		replies: false //filter out @replys
 	});
	*/

 	// Reposition Tweet bubble after a 3 second delay (time to load) so it is vertically centered no matter how many lines of text
	/*
	setTimeout(function() {
		var twitterBubbleHeight = $("#twitter").height();
		var twitterCalculatedTop = 65 -(twitterBubbleHeight * 0.5);
		var twitterArrowTop = (twitterBubbleHeight * 0.5) - 15;

		$("#twitter").css({"top": twitterCalculatedTop + "px"});
		$("#twitter .right-arrow").css({"top": twitterArrowTop + 8 + "px", "display":"block"});
		$("#twitter .right-arrow-border").css({"top": twitterArrowTop + 10 + "px", "display":"block"});
	}, 4000);
	*/

	// al3xandr3 twitter plugin - has a callback for when tweet is loaded!
	$('#tweets').twitter({'user': 'lemonagency', 'count': 1}, function (el) {
		$("#tweets div a").hide();
		var twitterBubbleHeight = $("#twitter").height();
		var twitterCalculatedTop = 65 -(twitterBubbleHeight * 0.5);
		var twitterArrowTop = (twitterBubbleHeight * 0.5) - 15;

		$("#twitter").css({"top": twitterCalculatedTop + "px"});
		$("#twitter .right-arrow").css({"top": twitterArrowTop + 8 + "px", "display":"block"});
		$("#twitter .right-arrow-border").css({"top": twitterArrowTop + 10 + "px", "display":"block"});
		//el.hide().show('slow');
	});

	/* ------------------------------------------------------------------------------------------------------------- */
	/* 0) HEADER - Lemon Logo / Vespa / H1 / H2 click behaviour														 */
	/* ------------------------------------------------------------------------------------------------------------- */

	var logoVisible = true;

	$("#lemon-logo, #vespa, h1 span, h2 span").click(function() {
		if(logoVisible == true){
			if($.browser.msie && $.browser.version < 9){
				$("h1, #lemon-logo").hide();
				$("h2, #vespa, #vespa-kickstand").show();
				$("#vespa, #vespa-kickstand").css({"top":"40px", "right":"120px"});
			} else{
				// Hide:
				$("h1").stop(true, false).animate({"opacity": "0", "filter":"0"}, 1000, "easeOutExpo");
				$("#lemon-logo").stop(true, false).animate({"opacity": "0", "filter":"0"}, 200, "easeOutExpo");
				// Show:
				$("h2").show().stop(true, false).animate({"opacity": "1", "filter":"0"}, 1000, "easeOutExpo");
				$("#vespa").show().stop(true, false).animate({"opacity": "1", "filter":"0", "top":"40px", "right":"120px"}, 600, "easeOutExpo");
				$("#vespa-kickstand").show().stop(true, false).delay(600).animate({"margin-top":"0"}, 300, "easeOutExpo");
			}
			logoVisible = false;
		} else {
			if($.browser.msie && $.browser.version < 9){
				$("h1, #lemon-logo").show();
				$("h2, #vespa, #vespa-kickstand").hide(0,resetHeader);
			} else{
				// Show:
				$("h1").show().stop(true, false).animate({"opacity": "1", "filter":"0"}, 1000, "easeInExpo");
				$("#lemon-logo").show().stop(true, false).delay(400).animate({"opacity": "1", "filter":"0"}, 700, "easeInExpo");
				// Hide:
				$("h2").stop(true, false).animate({"opacity": "0", "filter":"0"}, 900, "easeOutExpo");
				$("#vespa-kickstand").show().stop(true, false).animate({"margin-top":"-40px"}, 350, "easeOutExpo");
				$("#vespa").show().stop(true, false).delay(350).animate({"opacity": "0", "filter":"0", "top":"70px", "right":"60px"}, 600, "easeInBack", resetHeader);
			}
		}
	});

		function resetHeader(){
			$("h2").hide();
			$("#vespa").hide().css({"top":"20px", "right":"180px"});
			$("#vespa-kickstand").hide();
			logoVisible = true;
		}

	/* ------------------------------------------------------------------------------------------------------------ */
	/* 0) HEADER - MAIN NAV HOVERS																					*/
	/* ------------------------------------------------------------------------------------------------------------ */

		/* Nav hover anim WORK															*/
		/* ---------------------------------------------------------------------------- */

		$("li#nav-work a").hover(mouseOverWork, mouseOutWork);

		function mouseOverWork(){
			var randomNumberRotate= (Math.floor(Math.random()*50) -65);
			var randomNumberHeight= (Math.floor(Math.random()*30) + 144);
			$("#choppingboard").stop(true, false).animate({"top": "135px"}, 200, "easeInOutExpo");
			$("#knife").stop(true, false).animate({"top": randomNumberHeight, "rotate": randomNumberRotate }, 300, "easeOutBack");
		}

		function mouseOutWork(){
			$("#choppingboard").stop(true, false).animate({"top": "143px"}, 300, "easeInOutExpo");
			$("#knife").stop(true, false).animate({"top": "209px", "rotate": 0}, 200, "easeInOutExpo");
		}

		/* Nav hover anim CLIENTS														*/
		/* ---------------------------------------------------------------------------- */

		// Set up leaves
		function randomLeafPositionGenerate(){
			return (Math.floor(Math.random()*130) + 50);
		}

		function randomLeafRotationGenerate(){
			return (Math.floor(Math.random()*90) -45);
		}

		function resetLeaves(){
			$("#leaf1").css({"top": "170px", "left": randomLeafPositionGenerate(), "opacity": "1", "rotate": "0"});
			$("#leaf2").css({"top": "170px", "left": randomLeafPositionGenerate(), "opacity": "1", "rotate": "0"});
		}

		$("#leaf1").css({"left": randomLeafPositionGenerate() });
		$("#leaf2").css({"left": randomLeafPositionGenerate() });

		$("li#nav-clients a").hover(mouseOverClients, mouseOutClients);

		function mouseOverClients(){
			if($.browser.msie && $.browser.version < 9){
				// Shake is dodgy - element jumps down. Find a fix Lars!!
				//$("#tree").stop(true,false).effect("shake", { times:3 }, 50);
			} else {
				$("#tree").stop(true, false).animate({"rotate": "-5" }, 40, "easeOutBack")
															.animate({"rotate": "5" }, 90, "easeOutBack")
															.animate({"rotate": "-5" }, 40, "easeOutBack")
															.animate({"rotate": "5" }, 90, "easeOutBack")
															.animate({"rotate": "0" }, 150, "easeOutBack");
			}
			$("#leaf1").stop(true, false).animate({"top": "260px", "rotate": randomLeafRotationGenerate(), "opacity": "0" }, 1400, "easeOutExpo");
			$("#leaf2").stop(true, false).animate({"top": "260px", "rotate": randomLeafRotationGenerate(), "opacity": "0" }, 2400, "easeOutExpo", resetLeaves);
		}

		function mouseOutClients(){
			$("#leaf1").stop(true, false).css({"top": "160px", "left": randomLeafPositionGenerate(), "opacity": "1", "rotate": "0"});
			$("#leaf2").stop(true, false).css({"top": "160px", "left": randomLeafPositionGenerate(), "opacity": "1", "rotate": "0"});
		}

		/* Nav hover anim ABOUT															*/
		/* ---------------------------------------------------------------------------- */

		$("li#nav-about a").hover(mouseOverAbout, mouseOutAbout);

		function mouseOverAbout() {
			$("#lemon-simon").show().stop(true, false).animate({"left": "14px", "opacity": "1"}, 400, "easeOutBack");
			$("#lemon-knut").show().stop(true, false).animate({"left": "48px", "opacity": "1"}, 250, "easeOutBack");
			$("#lemon-gentlemon").stop(true, false).animate({"top": "117px"}, 200, "easeOutBack");
			$("#lemon-geir").show().stop(true, false).animate({"left": "126px", "opacity": "1"}, 300, "easeOutBack");
			$("#lemon-roald").show().stop(true, false).animate({"left": "155px", "opacity": "1"}, 350, "easeOutBack");
		}

		function mouseOutAbout() {
			if($.browser.msie && $.browser.version < 9){
				$("#lemon-simon").stop(true, false).animate({"left": "87px"}, 400, "easeOutBack", resetAbout);
				$("#lemon-knut").stop(true, false).animate({"left": "87px"}, 400, "easeOutBack");
				$("#lemon-gentlemon").stop(true, false).animate({"top": "133px"}, 500, "easeOutBounce");
				$("#lemon-geir").stop(true, false).animate({"left": "87px"}, 400, "easeOutBack");
				$("#lemon-roald").stop(true, false).animate({"left": "87px"}, 400, "easeOutBack");
			} else {
				$("#lemon-simon").stop(true, false).animate({"left": "87px", "opacity": "0"}, 400, "easeOutBack");
				$("#lemon-knut").stop(true, false).animate({"left": "87px", "opacity": "0"}, 400, "easeOutBack");
				$("#lemon-gentlemon").stop(true, false).animate({"top": "133px"}, 500, "easeOutBounce");
				$("#lemon-geir").stop(true, false).animate({"left": "87px", "opacity": "0"}, 400, "easeOutBack");
				$("#lemon-roald").stop(true, false).animate({"left": "87px", "opacity": "0"}, 400, "easeOutBack");
			}
		}
			function resetAbout(){
				$("#lemon-simon, #lemon-knut, #lemon-geir, #lemon-roald").hide();
			}

		/* Nav hover anim CONTACT														*/
		/* ---------------------------------------------------------------------------- */

		$("li#nav-contact a").hover(mouseOverContact, mouseOutContact);

		function mouseOverContact(){
			$("#phone").stop(true, false).animate({"left": "44px" }, 150, "easeInOutExpo");
			$("#receiver").stop(true, false).animate({"top": "94px", "left": "164px"}, 150, "easeInOutExpo");
		}

		function mouseOutContact(){
			$("#phone").stop(true, false).animate({"left": "64px" }, 150, "easeInOutExpo");
			$("#receiver").stop(true, false).animate({"top": "125px", "left": "141px"}, 300, "easeOutBounce");
		}


	/* ------------------------------------------------------------------------------------------------------------- */
	/* 1) WORK - Project Slideshows																					 */
	/* ------------------------------------------------------------------------------------------------------------- */

	/* Main content slideshow (Top level)											*/
	/* ---------------------------------------------------------------------------- */

	/*
	$("section#work div.content").slides({
		preload: true,
		preloadImage: "gfx/loading.gif",
		play: 0,
	    pause: 5000,
	    slideEasing: "easeInOutExpo",
	    slideSpeed: 600,
	    slidesLoaded: function() {
			$(".cycle-slideshow").css({"visibility":"visible"})
		},
	    bigTarget: false
	});
	*/

	$("section#work div.content .cycle-slideshow").cycle({
		delay: 2000,
		easing: "easeInOutExpo",
		fx: "scrollHorz",
		next: ".next",
		prev: ".prev",
		speed: 600,
		timeout: 0,
		cleartype: true,
		cleartypeNoBg: true
	});

	/* Image detail slideshow (Nested, per project)									*/
	/* ---------------------------------------------------------------------------- */

	$(".detail-slideshow").cycle({
		delay: 2000,
		next: ".detail-next",
		prev: ".detail-prev",
		speed: 500,
		timeout: 2000,
		cleartype: true,
		cleartypeNoBg: true
	});

	// Stop auto advance when clicking on detail slideshow (NOT WORKING FULLY - need to implement when clicking on buttons & video)
	$(".detail-slideshow").click(function() {
		$(".detail-slideshow").cycle('pause');
	});


	/* Project detail slideshow controls hover										*/
	/* ---------------------------------------------------------------------------- */

	$(".main-image > *").hover(mouseOverDetailSlideshow, mouseOutDetailSlideshow); // target everything inside .main-image div

	function mouseOverDetailSlideshow() {
		if($.browser.msie && $.browser.version < 9){
			$(this).parent().find(".detail-prev").show().css({"left":"18px"});
			$(this).parent().find(".detail-next").show().css({"right":"27px"});
			$(this).parent().find(".detail-zoom").show();
		} else {
			$(this).parent().find(".detail-prev").stop(true, false).show().animate({"left":"18px", "opacity":"0.9"}, 300, "easeOutExpo");
			$(this).parent().find(".detail-next").stop(true, false).show().animate({"right":"27px", "opacity":"0.9"}, 300, "easeOutExpo");
			$(this).parent().find(".detail-zoom").stop(true, false).show().animate({"opacity":"0.9"}, 300, "easeOutExpo");
		}
	}

	function mouseOutDetailSlideshow() {
		if($.browser.msie && $.browser.version < 9){
			$(this).parent().find(".detail-prev").css({"left":"5px", "display":"none"});
			$(this).parent().find(".detail-next").css({"right":"10px", "display":"none"});
			$(this).parent().find(".detail-zoom").hide();
		} else {
			$(this).parent().find(".detail-prev").stop(true, false).animate({"left":"5px", "opacity":"0"}, 300, "easeInExpo");
			$(this).parent().find(".detail-next").stop(true, false).animate({"right":"10px", "opacity":"0"}, 300, "easeInExpo");
			$(this).parent().find(".detail-zoom").stop(true, false).animate({"opacity":"0"}, 300, "easeInExpo", resetDetailSlideshow);
		}
	}

		function resetDetailSlideshow() {
			$(".detail-prev, .detail-next, .detail-zoom").hide();
		}

	// Make icons follow cursor vertically
	/*
	$(".detail-slideshow").mousemove(function(e){
			$(".detail-prev, .detail-next, .detail-zoom").css("top", e.clientY);
	});
	*/

	/* Experimental code for following the cursor from http://nnbox.ca/2010/03/jquery-nav-elements-that-follow-mouse-movement/ */
	/*
	var yTop; // distance wrapslide is from the top of the page
	jQuery.easing.def = "easeInOutQuart"; // apply easeInOutQuart easing

	function mouseFollow(e, id) { // mouseFollow() is called whenever the mouse enters a nav slider, id assigned to be slidePrev or slideNext
		var mouseYPos = e.pageY - yTop; // y-coord of mouse relative to wrapslide
		$('#readout').html('yTop =' + yTop + ', e.pageY' + e.pageY + ', mouseYPos = ' + mouseYPos); // display mouse tracking data, can be removed
		var heightBox = $(id + ' button').height(); // find height of clickable a tag
		var slideHeight = $(id).height(); // height of slidePrev or slideNext, the length of the whole slide-able area
		if (mouseYPos > 0 && mouseYPos < slideHeight ) { //ensure clickable a tag does not slide off #slidePrev or #slideNext
			var boxYPos; // this will track where the top of the a tag should be
			if (mouseYPos < (heightBox / 2)) { // prevents a tag from sliding above #slidePrev when the mouse is near the top of the div
				boxYPos = heightBox / 2; // force a tag to stay within div
			} else if (mouseYPos > slideHeight - (heightBox / 2)) { // prevents a tag from sliding below div when the mouse is near the bottom of the div
				boxYPos = slideHeight - (heightBox / 2); // force a tag to stay within div
			} else {
				boxYPos = mouseYPos;
			} // allow a tag to follow mouse if the mouse is in the middle of the div
			$(id + ' button').stop(true, true); // stops the mouseEnd() animation, so the a tag follows the mouse without delay.
			$(id + ' button').css('top', boxYPos - (heightBox / 2)); // - (heightBox/2) puts the mouse pointer in the center of the a tag.
		}
	}

	function mouseEnd(id) { // mouseEnd is called when the mouse leaves the nav slider
		var slideHeight = $(id).height();
		var heightBox = $(id + ' button').height();
		$(id + ' button').animate({top: (slideHeight/2) - (heightBox/2)}, 500); // a tag will ease back to the center of the div when the mouse leaves the div area
	}


	// grab distance wrapslide is from the top of the page
	yTop = $('.main-image').css('top');
	yTop = parseInt(yTop); // strips the unit from css property (e.g. 50px becomes 50)
	// tracks mouse movement inside slider area
	$('#.main-image').mousemove(function(e){
		mouseFollow(e, '.main-image');
	});
	// reset button location when pointer leaves slider area
	$('.main-image').mouseleave( function() {
		mouseEnd('.main-image');
	});
	*/



	/* Thumbnail Carousel															*/
	/* ---------------------------------------------------------------------------- */

	$("#projectthumbs").jCarouselLite({
        btnNext: ".next-thumbset",
        btnPrev: ".prev-thumbset",
        scroll: 1,
        visible: 4,
        easing: "easeInOutExpo",
        speed: 400,
        circular: false
    });

    // Hide thumbs on pageload by default
	$("#projectthumbs").hide();


		/* Work toggle btn behaviour (Toggle between main slideshow & thumbs)			*/
		/* ---------------------------------------------------------------------------- */

		var workToggle = "singleProject";
		$("div#work-toggle-btn button#project-single-btn").addClass("work-toggle-on");

		$("div#work-toggle-btn button#project-single-btn").click(function() {
			if(workToggle != "singleProject"){
				showSingleProject();
			}
		});

		$("div#work-toggle-btn button#project-thumbs-btn").click(function() {
			if(workToggle == "singleProject"){
				showProjectThumbgrid();
			}
		});

		function showSingleProject(){
			if($.browser.msie && $.browser.version < 9){
				// Show single project
				$("div#projectthumbs, section#work div.content button.prev-thumbset, section#work div.content button.next-thumbset").hide();
				$("section#work div.content div.cycle-slideshow, section#work div.content div.prev, section#work div.content div.next").show();
				// Hide category nav & change toggle btn
				$("section#work div.content nav#categories-nav").hide(0);
			} else {
				// Show single project
				$("div#projectthumbs, section#work div.content button.prev-thumbset, section#work div.content button.next-thumbset").fadeOut();
				$("section#work div.content div.cycle-slideshow, section#work div.content div.prev, section#work div.content div.next").fadeIn();
				// Hide category nav & change toggle btn
				$("section#work div.content nav#categories-nav").fadeOut(400);
			}

			$("#project-thumbs-label").addClass("label-inactive");
			$("#project-single-label").removeClass("label-inactive");

			// Change variable + classes
			$("div#work-toggle-btn button#project-single-btn").addClass("work-toggle-on");
			$("div#work-toggle-btn button#project-thumbs-btn").removeClass("work-toggle-on");
			workToggle = "singleProject";
		}

		function showProjectThumbgrid(){
			if($.browser.msie && $.browser.version < 9){
				// Show project thumbgrid
				$("div#projectthumbs").show().css({/* "width":"850px", "height":"120px" */});
				$("section#work div.content button.prev-thumbset, section#work div.content button.next-thumbset").show();
				$("section#work div.content div.cycle-slideshow, section#work div.content div.prev, section#work div.content div.next").hide();
				// Show category nav & change toggle btn
				$("section#work div.content nav#categories-nav").show();
			} else {
				// Show project thumbgrid
				$("div#projectthumbs").fadeIn().css({/* "width":"850px", "height":"120px" */});
				$("section#work div.content button.prev-thumbset, section#work div.content button.next-thumbset").fadeIn();
				$("section#work div.content div.cycle-slideshow, section#work div.content div.prev, section#work div.content div.next").fadeOut();
				// Show category nav & change toggle btn
				$("section#work div.content nav#categories-nav").fadeIn(400);
			}

			$("#project-thumbs-label").removeClass("label-inactive");
			$("#project-single-label").addClass("label-inactive");

			// Change variable + classes
			$("div#work-toggle-btn button#project-thumbs-btn").addClass("work-toggle-on");
			$("div#work-toggle-btn button#project-single-btn").removeClass("work-toggle-on");
			workToggle = "thumbGrid";

			// Animate Gentlemon
			//$("#gentlemon").stop(true, false).animate({"bottom":"0px", "height":"156px"}, 300, "easeInOutExpo")
											 //.animate({"bottom":"-50px", "height":"146px"}, 400, "easeOutBounce");
		}


		/* Work project thumbs hover													*/
		/* ---------------------------------------------------------------------------- */

		$("div#projectthumbs ul li div").hover(mouseOverThumb, mouseOutThumb);

		function mouseOverThumb(){
			$(this).children("h6, p").stop(true, false).css({"opacity": "0"});
			$(this).children("div.categories").stop(true, false).animate({"bottom": "-20px"}, 150, "easeInQuad");
		}

		function mouseOutThumb(){
			$(this).children("h6, p").stop(true, false).css({"opacity": "1"});
			$(this).children("div.categories").stop(true, false).animate({"bottom": "5px"}, 150, "easeOutQuad");
		}

		/* Work project thumbs click													*/
		/* ---------------------------------------------------------------------------- */

		$("div#projectthumbs ul li div").click(function() {
			showSingleProject();
		});

		/* "Related projects" pop-up menu on hover										*/
		/* ---------------------------------------------------------------------------- */

		$(".work-related-projects-btn, .work-related-projects-label, .related-projects-bubble").hover(mouseOverRelatedProjects, mouseOutRelatedProjects);

		function mouseOverRelatedProjects(){
			// Show and vertically position the related projects bubble according to button Y position
			var relatedBtnPos = $(".work-related-projects-btn").position();
			var bubbleYPos = (relatedBtnPos.top) - 24;
			$(this).parent().find(".related-projects-bubble").css({"display":"block", "top": bubbleYPos + "px"});
		}

		function mouseOutRelatedProjects(){
			$(this).parent().find(".related-projects-bubble").hide();
		}

		/* 'Lean' London Gentlemon when clicking right / left on the gallery			*/
		/* ---------------------------------------------------------------------------- */

		$("div.prev, button.prev-thumbset").click(function() {
			$("#gentlemon").animate({"rotate": "10"}, 250, "easeInExpo").animate({"rotate": "0"}, 250, "easeOutExpo");
		});

		$("div.next, button.next-thumbset").click(function() {
			$("#gentlemon").animate({"rotate": "-10"}, 250, "easeInExpo").animate({"rotate": "0"}, 250, "easeOutExpo");
		});


		/* London Gentlemon squeezy jump anim (add random text sayings)					*/
		/* ---------------------------------------------------------------------------- */

		$("#gentlemon").click(function() {
			var randomNumber = (Math.floor(Math.random()*90) -50);

			$(this).stop(true, false).animate({"height":"100px"}, 200, "easeInOutExpo")
									 .animate({"bottom":randomNumber, "height":"156px"}, 300, "easeInOutExpo")
									 .animate({"bottom":"-70px", "height":"146px"}, 300, "easeOutBounce");
		});


	/* ------------------------------------------------------------------------------------------------------------ */
	/* 2) CLIENTS																									*/
	/* ------------------------------------------------------------------------------------------------------------ */

		/* Client pop-up menu on hover													*/
		/* ---------------------------------------------------------------------------- */

		$("li.client").hover(mouseOverClient, mouseOutClient);

		function mouseOverClient(){
			$(this).children("div.client-popup").css({"display":"block"});

			// Calculate vertical position of pop-up bubble based on height (therefore based on number of items in list)
			var popupHeight = $(this).find("ul").height();
			var calculatedTop = -(popupHeight / 2) + 11;
			$(this).find("ul").css({"top":calculatedTop});
		}

		function mouseOutClient(){
			$(this).children("div.client-popup").hide();
		}

		// Highlight client website link in pop up menu when hovering just on the client name
		$("li.client a").hover(mouseOverClientLink, mouseOutClientLink);
		function mouseOverClientLink(){
			$(".client-site a").css({"color":"#433B27"});
		}
		function mouseOutClientLink(){
			$(".client-site a").css({"color":"#746F69"});
		}

	/* ------------------------------------------------------------------------------------------------------------ */
	/* 3) ABOUT - SUB NAVIGATION (THE COMPANY, OUR SERVICES, EMPLOYEES, JOBS, WORKING HERE)							*/
	/* ------------------------------------------------------------------------------------------------------------ */

	// On page load
	$(".tab-content").hide(); // Hide all content
	$("nav#aboutnav ul li:first-child").addClass("active").show(); // Activate first tab
	$(".tab-content:first-child").show(); // Show first tab content

	// On nav item click
	$("nav#aboutnav ul li").click(function() {

		$("nav#aboutnav ul li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab-content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content

		return false;
	});

		/* ABOUT > EMPLOYEES															*/
		/* ---------------------------------------------------------------------------- */

		// Phone number show / hide
		$(".btn-phone").click(function() {
			// Hide all others
			$(".employee div.btn-phone").stop(true, false).animate({"right": "88px"}, 300, "easeInOutExpo");
			$(".employee a").stop(true, false).animate({"left": "89px"}, 300, "easeInOutExpo");
			$(".employee p").hide(300);

			// Show selected number
			$(this).stop(true, false).animate({"right": "38px"}, 300, "easeInOutExpo");
			$(this).parent().find("a").stop(true, false).animate({"left": "35px"}, 300, "easeInOutExpo");
			$(this).parent().find("p").show(300);
		});

		// Squeezy employee lemon jump (when lemon lands, card flip is triggered!)
		$("li.employee .card.front img").click(function() {
			var lemonWidth = $(this).width();
			var lemonHeight = $(this).height();
			var lemonSqueezed = lemonHeight - 20;
			var lemonStretched = lemonHeight + 16;
			$(this).css({"width": lemonWidth}); //stops lemon from being squeezed horizontally too

			if(lemonHeight >= 90){ //stop lemons from shrinking indefinitely from quick repeated clicking
				$(this).stop(true, false).animate({"height": lemonSqueezed}, 200, "easeInOutExpo")
										 .animate({"padding-bottom":"30px", "height": lemonStretched}, 300, "easeOutExpo")
										 .animate({"padding-bottom":"0", "height": lemonHeight}, 300, "easeOutBounce", cardFlip);
			}

			// 3D flip yo!
			function cardFlip(){
				$(this).parent().parent().addClass("flip");
			}
		});

		$("li.employee .card.back img").click(function() {
			$(this).parent().parent().removeClass("flip");
		});


		/* ABOUT > WORKING HERE / LIVING HERE Photo slideshow							*/
		/* ---------------------------------------------------------------------------- */

		$("#workinghere .cycle-slideshow").cycle({
			delay: 2000,
			easing: "easeInOutExpo",
			fx: "scrollHorz",
			next: "#workinghere .next",
			prev: "#workinghere .prev",
			pager: "#workinghere .pager",
			speed: 600,
			timeout: 5000,
			cleartype: true,
			cleartypeNoBg: true
		});

			// Caption Hover
			$("#workinghere .slide, #workinghere .prev, #workinghere .next").hover(slideOver, slideOut);

			function slideOver(){
				$(".caption").stop(true, false).animate({"bottom":"0"}, 400, "easeInOutExpo");
			}

			function slideOut(){
				$(".caption").stop(true, false).animate({"bottom":"-140px"}, 400, "easeInOutExpo");
			}


	/* ------------------------------------------------------------------------------------------------------------- */
	/* 4) FOOTER - PARALLAX SHIZ (On mouse move)																 	 */
	/* ------------------------------------------------------------------------------------------------------------- */

	function positionHills() {
		// Work out half the viewport width and subtract by half the width of the hill to determine left position (centering)
		var viewportWidth = $(window).width();
		$("#near-hill").css({"left": ((viewportWidth* 0.5) - 1060)});
		$("#mid-hill").css({"left": ((viewportWidth * 0.5) - 1238)});
		$("#far-hill").css({"left": ((viewportWidth * 0.5) - 1050)});
		$("#slottsfjell").css({"left": ((viewportWidth * 0.5) - 469)});
		$("#vikingbow").css({"left": ((viewportWidth * 0.5) - 15)});
		$("#lemon-arnie").css({"left": ((viewportWidth * 0.5) - 463)});
		$("#arnie-bubble").css({"left": ((viewportWidth * 0.5) - 560)});
		$("#arnie-btn").css({"left": ((viewportWidth * 0.5) - 460)});
		$("#cloud1").css({"left": ((viewportWidth* 0.5) + 40)});
		$("#cloud2").css({"left": ((viewportWidth * 0.5) - 615)});
		$("#cloud3").css({"left": ((viewportWidth * 0.5) - 67)});
		$("#cloud4").css({"left": ((viewportWidth * 0.5) - 477)});

		$("#near-hill").plaxifyXOnly({"xRange":200,"invert":true});
		$("#mid-hill").plaxifyXOnly({"xRange":100,"invert":true});
		$("#far-hill").plaxifyXOnly({"xRange":60,"invert":true});
		$("#slottsfjell").plaxifyXOnly({"xRange":60,"invert":true});
		$("#vikingbow").plaxifyXOnly({"xRange":60,"invert":true});
		//$("#lemon-arnie").plaxifyXOnly({"xRange":30,"invert":true});
		$("#arnie-btn").plaxifyXOnly({"xRange":30,"invert":true});
		$.plax.enable();
	}

	$(window).resize(function() {
		positionHills();
	});

	$('#cloud1').plaxify({"xRange":25,"yRange":25,"invert":true});
	$('#cloud2').plaxify({"xRange":5,"yRange":5,"invert":true});
	$('#cloud3').plaxify({"xRange":5,"yRange":5,"invert":true});
	$('#cloud4').plaxify({"xRange":5,"yRange":5,"invert":true});

	$("#vikingarrow1").plaxify({"xRange":600,"yRange":60,"invert":true});
	$("#vikingarrow2").plaxify({"xRange":500,"yRange":20,"invert":true});
	$("#vikingarrow3").plaxify({"xRange":400,"yRange":10,"invert":true});
	$("#vikingarrowblur1").plaxify({"xRange":2500,"yRange":15,"invert":true});
	$("#vikingarrowblur2").plaxify({"xRange":1600,"yRange":20,"invert":true});
	$("#vikingarrowblur3").plaxify({"xRange":800,"yRange":250,"invert":true});
	$.plax.enable();

	positionHills();

	/* ------------------------------------------------------------------------------------------------------------- */
	/* 4) FOOTER - FUN STUFF																					 	 */
	/* ------------------------------------------------------------------------------------------------------------- */

	/* Viking attacker clicks														*/
	/* ---------------------------------------------------------------------------- */

	$("#vikingattacker").click( function() {
		$(this).stop(true, false).animate({"left":"1160px"}, 300, "easeOutBounce").animate({"left":"1090px"}, 200, "easeInOutExpo");
	});

	$("#vikingattacker2").click( function() {
		$(this).stop(true, false).animate({"left":"1210px"}, 300, "easeOutBounce").animate({"left":"1280px"}, 200, "easeInOutExpo");
	});

	/* Arnie Easter egg																*/
	/* ---------------------------------------------------------------------------- */

	var arnieOrigX;
	var arnieHoverX;
	var arnieActive = false;

	$("#arnie-btn, #lemon-arnie").hover(mouseOverArnie, mouseOutArnie);

	function mouseOverArnie(){
		if(!arnieActive) {
			var viewportWidth = $(window).width();
			arnieOrigX = ((viewportWidth * 0.5) - 458);
			arnieHoverX = arnieOrigX - 30;
			$("#arnie-btn, #lemon-arnie").stop(true, false).animate({"left":arnieHoverX}, 200, "easeOutExpo");
		}
	}

	function mouseOutArnie(){
		if(!arnieActive){
			$("#arnie-btn, #lemon-arnie").stop(true, false).animate({"left":arnieOrigX}, 200, "easeInExpo");
		}
	}

	$("#arnie-btn").click( function() {
		if(!arnieActive){
			arnieOnPos = arnieHoverX - 25;
			$("#arnie-btn, #lemon-arnie").stop(true, false).animate({"left":arnieOnPos}, 200, "easeInExpo");
			arnieActive = true;

			//Randomise phrases:
			var randomPhrase = (Math.floor(Math.random()*4));
			switch (randomPhrase) {
				case 0:
					$("#arnie-bubble p").text("Come with me if you want to live!");
					break;
				case 1:
					$("#arnie-bubble p").text("I'll be back.");
					break;
				case 2:
					$("#arnie-bubble p").text("Hasta la vista, baby!");
					break;
				case 3:
					$("#arnie-bubble p").text("Your clothes... give them to me, now.");
					break;
				default:
				$("#arnie-bubble p").text("Come with me if you want to live!");
			}

			$("#arnie-bubble").fadeIn(400, "easeOutQuad").delay(2000).fadeOut(400, "easeInQuad", function() {
				$("#arnie-btn, #lemon-arnie").stop(true, false).animate({"left":arnieOrigX}, 200, "easeInExpo");
				arnieActive = false;
			});
		}
	});

});